﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TEF.Composition
{
    /// <summary>
    /// An attribute to mark that a class is to be exported in association to some type, usually an interface.  The system reads these attributes and inserts into the dependency container the interface and class mappings.  If there are multiple classes mapped to the same interface, these are mapped as a one-to-many situation.
    /// </summary>
	[System.Diagnostics.DebuggerDisplay("Contract: {ContractType.Name}, Name: {Name}")]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=true, Inherited=true)]
    public class ExportTypeAttribute : Attribute
    {

		/// <summary>
		/// Gets or sets the type of interface.
		/// </summary>
      public Type ContractType { get; private set; }

		/// <summary>
		/// Gets the scope of the object.  Defaults to Singleton.
		/// </summary>
		public LifeTimeScope LifeTimeScope { get; set; }

		/// <summary>
		/// Gets the name of the custom lifetime scope.
		/// </summary>
		public string LifeTimeScopeCustomName { get; set; }

		/// <summary>
		/// Gets or sets a name to register this as a named registration.  This is optional.
		/// </summary>
		public string Name { get; set; }



		/// <summary>
		/// Marks the type for export, to be exported as a given contract.  Can be defined multiple times on an object.
		/// </summary>
		/// <param name="contractType">The type to be exported as.</param>
      public ExportTypeAttribute(Type contractType)
      {
         this.ContractType = contractType;
      }

      public ExportTypeAttribute(Type contractType, string name)
         : this(contractType)
      {
         this.Name = name;
      }

      public ExportTypeAttribute(Type contractType, LifeTimeScope scope, string customLifetimeScopeName = null)
			: this(contractType)
		{
			this.LifeTimeScope = scope;

         if (this.LifeTimeScope == LifeTimeScope.Custom)
            this.LifeTimeScopeCustomName = customLifetimeScopeName;
		}

      public ExportTypeAttribute(Type contractType, string name, LifeTimeScope scope, string customLifetimeScopeName = null)
         : this(contractType, scope, customLifetimeScopeName)
      {
         this.Name = name;
      }

   }
}
