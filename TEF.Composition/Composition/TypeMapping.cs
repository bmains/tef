﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TEF.Composition
{
   /// <summary>
   /// Represents the type mapping used to contain the registrations found.
   /// </summary>
   [System.Diagnostics.DebuggerDisplay("Contract={ContractType.Name}, Implementation={ImplementationType.Name}, Name={Name}")]
   public class TypeMapping
   {

      /// <summary>
      /// Gets the contract type (also refered to as the interface)
      /// </summary>
      public Type ContractType { get; set; }

      /// <summary>
      /// Gets or sets a name to use for a named registration.  This is only used if the underlying container supports it.
      /// </summary>
      public string Name { get; set; }

      /// <summary>
      /// Gets or sets the lifetime of the scoping of this object.
      /// </summary>
      public List<TypeImplementationRegistration> Implementations { get; set; } = new List<TypeImplementationRegistration>();
   }

}
