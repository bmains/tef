﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace TEF.Composition
{
   /// <summary>
   /// Used to find within an assembly any instance that defines a <see cref="ExportDependencyAttribute"/> attribute marker.
   /// </summary>
   public interface IExportDependencyFinder
   {

      /// <summary>
      /// Used to find within an assembly any instance that defines a <see cref="ExportDependencyAttribute"/> attribute marker.
      /// </summary>
      /// <param name="assembly">The assembly to inspect.</param>
      /// <returns>The list of type mappings.</returns>
      IEnumerable<TypeMapping> GetAssemblyTypes(Assembly assembly);


   }
}
