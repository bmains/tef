﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEF.Composition
{
   public class TypeImplementationScope
   {
      public string CustomScopeName
      {
         get;
         private set;
      }

      public LifeTimeScope Scope
      {
         get;
         private set;
      }

      public static bool operator ==(TypeImplementationScope scope, string scopeKey)
      {
         if (Object.Equals(scope, null))
            return false;

         return Object.Equals(scope.GetScopeKey(), scopeKey);
      }

      public static bool operator ==(TypeImplementationScope scope, TypeImplementationScope otherScope)
      {
         if (Object.Equals(scope, null) != Object.Equals(otherScope, null))
            return false;

         return Object.Equals(scope.GetScopeKey(), otherScope.GetScopeKey());
      }

      public static bool operator !=(TypeImplementationScope scope, string scopeKey)
      {
         if (Object.Equals(scope, null))
            return true;

         return !Object.Equals(scope.GetScopeKey(), scopeKey);
      }

      public static bool operator !=(TypeImplementationScope scope, TypeImplementationScope otherScope)
      {
         if (Object.Equals(scope, null) != Object.Equals(otherScope, null))
            return true;

         return !Object.Equals(scope.GetScopeKey(), otherScope.GetScopeKey());
      }



      /// <summary>
      /// Used only for testing purposes.
      /// </summary>
      protected TypeImplementationScope() {  }

      public TypeImplementationScope(LifeTimeScope scope)
      {
         if (scope == LifeTimeScope.Custom)
            throw new ArgumentException("When using a custom scope, use the [string] constructor override, which takes a custom scope name instead.", "scope");

         this.Scope = scope;
      }

      public TypeImplementationScope(string customScopeName)
      {
         if (string.IsNullOrEmpty(customScopeName))
            throw new ArgumentNullException("customScopeName");

         this.Scope = LifeTimeScope.Custom;
         this.CustomScopeName = customScopeName;
      }



      public override bool Equals(object obj)
      {
         if (obj == null)
            return false;
         if (obj is string)
            return ((string)obj) == this.GetScopeKey();
         else if (obj is TypeImplementationScope)
            return ((TypeImplementationScope)obj).GetScopeKey() == this.GetScopeKey();
         else
            return false;
      }

      public override int GetHashCode()
      {
         return this.GetScopeKey().GetHashCode();
      }

      public string GetScopeKey()
      {
         if (this.Scope == LifeTimeScope.Custom)
            return "Custom_" + (CustomScopeName ?? "Default");
         else
            return Enum.GetName(typeof(LifeTimeScope), this.Scope);
      }

   }
}
