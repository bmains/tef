﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEF.Composition
{
   /// <summary>
   /// Represents the dependency defined at the assembly level.  Some mappings can't be defined manually, especially if the dependency is in an external third-party library.  This type of attribute can help bridge the gap.  Additionally, the system reads these attributes and inserts into the dependency container the interface and class mappings.  If there are multiple classes mapped to the same interface, these are mapped as a one-to-many situation.
   /// </summary>
   /// <example>
   /// [ExportDependency(typeof(ISomeThirdPartyComponent), typeof(SomeThirdPartyComponent))]
   /// [ExportDependency(typeof(ISomeThirdPartyComponent), typeof(SomeThirdPartyComponent), LifeTimeScope.Transient)]
   /// </example>
   [System.Diagnostics.DebuggerDisplay("Contract: {ContractType.Name}, Implementation: {ImplementationType.Name}, Name: {Name}")]
   [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
   public class ExportDependencyAttribute : Attribute
   {
      /// <summary>
      /// Gets or sets the contract type (interface or abstract class).
      /// </summary>
      public Type ContractType { get; private set; }

      /// <summary>
      /// Gets the implementation type that implements the contract.
      /// </summary>
      public Type ImplementationType { get; private set; }

      /// <summary>
      /// Gets the scope of the object.  Defaults to Singleton.
      /// </summary>
      public LifeTimeScope LifeTimeScope { get; set; }

      /// <summary>
      /// Gets the name of the custom lifetime scope.
      /// </summary>
      public string LifeTimeScopeCustomName { get; set; }

      /// <summary>
      /// Gets or sets a name to register this as a named registration.  This is optional.
      /// </summary>
      public string Name { get; set; }



      /// <summary>
      /// Creates the dependency with the contract and implementation.  Optionally, a name for the dependency can be provided.
      /// </summary>
      /// <param name="contractType">The type of contract.</param>
      /// <param name="implementationType">The type of implementation using the contract.</param>
      public ExportDependencyAttribute(Type contractType, Type implementationType)
      {
         this.ContractType = contractType;
         this.ImplementationType = implementationType;
      }
      
      /// <summary>
      /// Creates the dependency with the contract and implementation.  Optionally, a name for the dependency can be provided.
      /// </summary>
      /// <param name="contractType">The type of contract.</param>
      /// <param name="implementationType">The type of implementation using the contract.</param>
      /// <param name="name">The name for a named registration.</param>
      public ExportDependencyAttribute(Type contractType, Type implementationType, string name)
         : this(contractType, implementationType)
      {
         this.Name = name;
      }

      /// <summary>
      /// Creates the dependency with the contract and implementation.  Optionally, a name for the dependency can be provided.
      /// </summary>
      /// <param name="contractType">The type of contract.</param>
      /// <param name="implementationType">The type of implementation using the contract.</param>
      /// <param name="lifetimeScope">The lifetime scope of the registration.</param>
      public ExportDependencyAttribute(Type contractType, Type implementationType, LifeTimeScope lifetimeScope, string customLifetimeScopeName = null)
         : this(contractType, implementationType)
      {
         this.LifeTimeScope = lifetimeScope;
         if (this.LifeTimeScope == LifeTimeScope.Custom)
            this.LifeTimeScopeCustomName = customLifetimeScopeName;
      }

      /// <summary>
      /// Creates the dependency with the contract and implementation.  Optionally, a name for the dependency can be provided.
      /// </summary>
      /// <param name="contractType">The type of contract.</param>
      /// <param name="implementationType">The type of implementation using the contract.</param>
      /// <param name="name">The name for a named registration.</param>
      /// <param name="lifetimeScope">The lifetime scope of the registration.</param>
      public ExportDependencyAttribute(Type contractType, Type implementationType, string name, LifeTimeScope lifetimeScope, string customLifetimeScopeName = null)
         : this(contractType, implementationType, lifetimeScope)
      {
         this.Name = name;

         if (this.LifeTimeScope == LifeTimeScope.Custom)
            this.LifeTimeScopeCustomName = customLifetimeScopeName;
      }

      

   }
}
