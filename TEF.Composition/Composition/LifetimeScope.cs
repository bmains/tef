﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TEF.Composition
{

   /// <summary>
   /// Every container has different rules around the scope of object it maintains; this can be difficult to maintain in a common framework.  However, it does seem that the following included are the more common ones, and custom lifecycles can be created to fill in the gap.
   /// </summary>
	public enum LifeTimeScope
	{
      /// <summary>
      /// Object is stored in singleton scope; it's created once, and it lives the entire life of the application.
      /// </summary>
		Singleton,
      /// <summary>
      /// Every time this object is requested, it's re-created every single time.
      /// </summary>
		Transient,
      /// <summary>
      /// The object is created on a per-thread basis.
      /// </summary>
		PerThread,
      /// <summary>
      /// The object is created on a per-request basis, which is usually only for applications that are stateless (such as ASP.NET).  It's up to each implementation to control this lifecycle; if the scope doesn't exist, it will probably function similarly to a Per-Thread basis.
      /// </summary>
		PerRequest,
      /// <summary>
      /// The object is created on a custom basis, which is managed in a manual fashion.  This requires a name associated to the custom lifecycle.
      /// </summary>
		Custom
	}

}
