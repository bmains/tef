﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace TEF.Composition
{
   /// <summary>
   /// Represents the default implementation of the <see cref="IMetadataImporter"/> that imports metadata, using the System.Reflection namespace to find types defined in an assembly or array of types to load into the DI container.
   /// </summary>
   public class TEFMetadataImporter : IMetadataImporter
   {
      private IExportDependencyFinder _exportDependencyFinder = null;
      private IExportTypeFinder _exportTypeFinder = null;



      protected TEFMetadataImporter()
      {
         //Only used for MOCKING
      }

      public TEFMetadataImporter(IExportDependencyFinder depFinder, IExportTypeFinder typeFinder)
      {
         _exportDependencyFinder = depFinder;
         _exportTypeFinder = typeFinder;
      }


      
      /// <summary>
      /// Creates an initialized TEF metadata importer with the default implemented classes.
      /// </summary>
      /// <returns></returns>
      public static TEFMetadataImporter CreateDefault()
      {
         return new TEFMetadataImporter(new ReflectionExportDependencyFinder(), new ReflectionExportTypeFinder());
      }

      public IEnumerable<TypeMapping> GetAssemblyTypes(Assembly assembly)
      {
         if (assembly == null)
            throw new ArgumentNullException("assembly");

         return ParseAssemblyForAssemblyAttributes(assembly);
      }

      public IEnumerable<TypeMapping> GetAssemblyTypes(IEnumerable<Assembly> assemblies)
      {
         if (assemblies == null)
            throw new ArgumentNullException("assemblies");

         var list = new List<TypeMapping>();

         foreach (var a in assemblies)
            list.AddRange(GetAssemblyTypes(a));

         return list;
      }

      public IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Type> types)
      {
         if (types == null)
            throw new ArgumentNullException("types");

         try
         {
            return ParseTypeForAttribute(types);
         }
         catch (System.Reflection.ReflectionTypeLoadException tlex)
         {
            string msg = String.Join(Environment.NewLine, tlex.LoaderExceptions.Select(i => i.StackTrace));
            throw new InvalidOperationException("Unable to load class types:  " + tlex.Message + "  The load errors are: " + Environment.NewLine + msg);
         }
      }

      public IEnumerable<TypeMapping> GetClassTypes(Assembly assembly)
      {
         if (assembly == null)
            throw new ArgumentNullException("assembly");

         try
         {
            return ParseTypeForAttribute(assembly.GetTypes());
         }
         catch (System.Reflection.ReflectionTypeLoadException tlex)
         {
            string msg = String.Join(Environment.NewLine, tlex.LoaderExceptions.Select(i => i.Message));
            throw new InvalidOperationException("Unable to load class types in assembly " + assembly.FullName + ": " + tlex.Message + "  The load errors are:" + Environment.NewLine + msg);
         }
      }

      public IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Assembly> assemblies)
      {
         var list = new List<TypeMapping>();

         foreach (var a in assemblies)
         {
            list.AddRange(GetClassTypes(a));
         }

         return list;
      }
      
      protected virtual IEnumerable<TypeMapping> ParseAssemblyForAssemblyAttributes(Assembly assembly)
      {
         return _exportDependencyFinder.GetAssemblyTypes(assembly);

         //return assembly.GetCustomAttributes(typeof(ExportDependencyAttribute), false)
         //   .Cast<ExportDependencyAttribute>()
         //   .GroupBy(i => new { i.ContractType, i.ImplementationType, i.Name })
         //   .Select(i => new TypeMapping
         //   {
         //      ContractType = i.Key.ContractType,
         //      ImplementationType = i.Key.ImplementationType,
         //      Name = i.Key.Name,
         //      LifetimeScope = i.Select(ls => new LifeTimeScopeRegistration { Scope = ls.LifeTimeScope, CustomScopeName = ls.LifeTimeScopeCustomName }).Distinct().ToList()
         //   });
      }

      protected virtual IEnumerable<TypeMapping> ParseTypeForAttribute(IEnumerable<Type> types)
      {
         return _exportTypeFinder.GetClassTypes(types);

         //var mappings = new List<TypeMapping>();

         //foreach (var type in types)
         //{
         //   var attrib = type.GetCustomAttributes(typeof(ExportTypeAttribute), false).FirstOrDefault() as ExportTypeAttribute;

         //   if (attrib != null)
         //   {
         //      var mapping = mappings.FirstOrDefault(i => i.ContractType == attrib.ContractType &&
         //                                i.ImplementationType == type &&
         //                                i.Name == attrib.Name);

         //      if (mapping == null)
         //      {
         //         mappings.Add(new TypeMapping
         //         {
         //            ContractType = attrib.ContractType,
         //            ImplementationType = type,
         //            Name = attrib.Name,
         //            LifetimeScope = new List<LifeTimeScopeRegistration>() { new LifeTimeScopeRegistration { Scope = attrib.LifeTimeScope, CustomScopeName = attrib.LifeTimeScopeCustomName } }
         //         });
         //      }
         //      else
         //      {
         //         var potentialNewReg = new LifeTimeScopeRegistration { Scope = attrib.LifeTimeScope, CustomScopeName = attrib.LifeTimeScopeCustomName };

         //         if (!mapping.LifetimeScope.Any(i => i.GetKey() == potentialNewReg.GetKey()))
         //            mapping.LifetimeScope.Add(potentialNewReg);
         //      }
         //   }
         //}

         //return mappings;
      }


   }
}