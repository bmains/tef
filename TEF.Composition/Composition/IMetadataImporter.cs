﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TEF.Composition
{
   /// <summary>
   /// Represents the importer of metadata.  Imports metadata by assembly attributes, or by inspecting the class themselves.
   /// </summary>
   /// <remarks>There is a default implementation available with the following class: <see cref="TEFMetdataImporter"/>.</remarks>
   public interface IMetadataImporter
   {

      /// <summary>
      /// Gets the mappings defined in a single assembly, which is searching for assembly-based [ExportDependency] attributes.
      /// </summary>
      /// <param name="assembly">The assembly to search.</param>
      /// <returns>The mappings found.</returns>
      IEnumerable<TypeMapping> GetAssemblyTypes(Assembly assembly);

      /// <summary>
      /// Gets the mappings defined in a list of assemblies, which is searching for assembly-based [ExportDependency] attributes.
      /// </summary>
      /// <param name="assemblies">The assembly to search.</param>
      /// <returns>The mappings found.</returns>
      IEnumerable<TypeMapping> GetAssemblyTypes(IEnumerable<Assembly> assemblies);

      /// <summary>
      /// Gets the mappings defined in an array of types, which are marked with the [ExportType] attribute.  These types are not instantiated.
      /// </summary>
      /// <param name="types">The list of types to search.</param>
      /// <returns>The mappings found.</returns>
      IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Type> types);

      /// <summary>
      /// Gets the mappings defined in a single assembly, which are marked with the [ExportType] attribute.  These types are not instantiated.
      /// </summary>
      /// <param name="assembly">The assembly to search.</param>
      /// <returns>The mappings found.</returns>
      IEnumerable<TypeMapping> GetClassTypes(Assembly assembly);

      /// <summary>
      /// Gets the mappings defined in a list of assemblies, which are marked with the [ExportType] attribute.  These types are not instantiated.
      /// </summary>
      /// <param name="assemblies">The list of assemblies to search.</param>
      /// <returns>The mappings found.</returns>
      IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Assembly> assemblies);

   }
}
