﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEF.Composition
{
   /// <summary>
   /// Represents a specific implementation registration against a given contract, which could be one-to-one or one-to-many.
   /// </summary>
   public class TypeImplementationRegistration
   {

      /// <summary>
      /// Gets the implementation type, or the underlying object mapped to the given contract/interface.
      /// </summary>
      public Type ImplementationType { get; set; }

      /// <summary>
      /// Gets or sets the scope of the object, which dictacts how long an object lives within the container.
      /// </summary>
      public TypeImplementationScope ScopeLifetime
      {
         get;
         set;
      }

   }
   
}
