﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEF.Composition
{
   /// <summary>
   /// Used to find types within an assembly that implements the <see cref="IExportTypeFinder"/> as a class-level attribute.
   /// </summary>
   public interface IExportTypeFinder
   {

      /// <summary>
      /// Used to find types within an assembly that implements the <see cref="IExportTypeFinder"/> as a class-level attribute.
      /// </summary>
      /// <param name="types">The list of types to search.</param>
      /// <returns>The found mappings.</returns>
      IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Type> types);

   }
}
