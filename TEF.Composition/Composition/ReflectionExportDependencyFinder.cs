﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TEF.Composition
{
   public class ReflectionExportDependencyFinder : IExportDependencyFinder
   {
      public virtual List<TypeImplementationRegistration> GetImplementations(Type contractType, string name, IEnumerable<ExportDependencyAttribute> attribs)
      {
         var list = new List<TypeImplementationRegistration>();
         var matchedAttribs = attribs.Where(i => i.ContractType == contractType && i.Name == name).ToList();

         foreach (var attrib in  matchedAttribs)
         {
            list.Add(new TypeImplementationRegistration
            {
               ImplementationType = attrib.ImplementationType,
               ScopeLifetime = (attrib.LifeTimeScope == LifeTimeScope.Custom) 
                  ? new TypeImplementationScope(attrib.LifeTimeScopeCustomName) 
                  : new TypeImplementationScope(attrib.LifeTimeScope)
            });
         }

         return list;
      }

      public IEnumerable<TypeMapping> GetAssemblyTypes(Assembly assembly)
      {
         var attribs = GetAttributes(assembly);

         return attribs
            .GroupBy(i => new { i.ContractType, i.Name })
            .Select(i => new TypeMapping
            {
               ContractType = i.Key.ContractType,
               Name = i.Key.Name,
               Implementations = GetImplementations(i.Key.ContractType, i.Key.Name, attribs)
            });
      }

      public virtual IEnumerable<ExportDependencyAttribute> GetAttributes(Assembly assembly)
      {
         return assembly.GetCustomAttributes(typeof(ExportDependencyAttribute), false).Cast<ExportDependencyAttribute>();
      }

   }
}
