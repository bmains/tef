﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TEF.Composition
{
   public class ReflectionExportTypeFinder : IExportTypeFinder
   {
      
      public virtual TypeImplementationRegistration GetImplementation(Type implementationType, ExportTypeAttribute attribute)
      {
         return new TypeImplementationRegistration
         {
            ImplementationType = implementationType,
            ScopeLifetime = (attribute.LifeTimeScope == LifeTimeScope.Custom)
                  ? new TypeImplementationScope(attribute.LifeTimeScopeCustomName)
                  : new TypeImplementationScope(attribute.LifeTimeScope)
         };
      }

      public IEnumerable<TypeMapping> GetClassTypes(IEnumerable<Type> types)
      {
         var mappings = new List<TypeMapping>();

         foreach (var type in types)
         {
            var attrib = GetAttributeForType(type);

            if (attrib != null)
            {
               TypeMapping mapping = mappings.FirstOrDefault(i => i.ContractType == attrib.ContractType && i.Name == attrib.Name);

               if (mapping == null)
               {
                  mapping = new TypeMapping
                  {
                     ContractType = attrib.ContractType,
                     Name = attrib.Name
                  };

                  mappings.Add(mapping);
               }

               mapping.Implementations.Add(GetImplementation(type, attrib));
            }
         }

         return mappings;
      }

      public virtual ExportTypeAttribute GetAttributeForType(Type type)
      {
         return type.GetCustomAttributes(typeof(ExportTypeAttribute), false).FirstOrDefault() as ExportTypeAttribute;
      }


   }
}
