﻿using System;
using System.Collections.Generic;
using System.Linq;

using TEF.Composition;


namespace TEF.Consumption
{
	/// <summary>
	/// Represents the factory that constructs the types that the container will use.
	/// </summary>
	public class TypeFactory
	{
		protected internal static IAssemblyLoader _assemblyLoader = null;
		protected internal static IConsumer _consumer = null;
		protected internal static IMetadataImporter _metdataImporter = null;



		protected TypeFactory() { }



		/// <summary>
		/// Configures the type factory; the Consumer and the AssemblyImporter are required operations.  The MetadataImporter is defaulted to <see cref="TEFMetadataImporter" />.
		/// </summary>
		/// <param name="fn">The configuration routine; the AssemblyLoader and Consumer is required to be provided by the application; the MetadataImporter is defaulted.</param>
		/// <returns>The type factory.</returns>
		public static TypeFactory Configure(Action<TypeFactoryConfiguration> fn)
		{
         lock (typeof(TypeFactory))
         {
            var config = new TypeFactoryConfiguration();
            fn(config);

            _assemblyLoader = config.GetAssemblyLoader();
            _consumer = config.GetConsumer();
            _metdataImporter = config.GetMetadataImporter() ?? TEFMetadataImporter.CreateDefault();

            if (_assemblyLoader == null)
               throw new InvalidOperationException("An assembly loader has not been configured.  This is required.");
            if (_consumer == null)
               throw new InvalidOperationException("An consumer has not been configured.  This is required.");
         }

			return new TypeFactory();
		}

		/// <summary>
		/// Initializes the types and consumes them, returning the container in a weakly-typed form.
		/// </summary>
      /// <returns>The container, in object form.</returns>
		public object Initialize()
		{
			var assemblies = _assemblyLoader.LoadAssemblies();
			var typeList = _metdataImporter.GetAssemblyTypes(assemblies).ToList();
			typeList.AddRange(_metdataImporter.GetClassTypes(assemblies));

			return _consumer.Consume(typeList);
		}

      /// <summary>
      /// Initializes the types and consumes them, returning the container in a weakly-typed form.
      /// </summary>
      /// <param name="postInit">Initializes the container after the types have been loaded into it.</param>
      /// <returns>The container, in object form.</returns>
      public object Initialize(Action<object> postInit)
      {
         var assemblies = _assemblyLoader.LoadAssemblies();
         var typeList = _metdataImporter.GetAssemblyTypes(assemblies).ToList();
         typeList.AddRange(_metdataImporter.GetClassTypes(assemblies));

         var container = _consumer.Consume(typeList);
         postInit(container);

         return container;
      }


	}
}
