﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace TEF.Consumption
{
   /// <summary>
   /// Responsible for loading assemblies; this is useful because it can control which assemblies to load, omitting framework components like "System", "Microsoft.*" components, etc.
   /// </summary>
   /// <example>
   /// public class NonSystemAppDomainAssemblyLoader : IAssemblyLoader
   ///{
   ///	protected virtual IEnumerable<Assembly> FilterAssemblies(IEnumerable&lt;Assembly&gt; assemblies)
   ///   {
   ///      return assemblies;
   ///   }
   ///
   ///   public IEnumerable&lt;Assembly&gt; LoadAssemblies()
   ///   {
   ///      return FilterAssemblies(AppDomain.CurrentDomain.GetAssemblies().Where(i =&gt;
   ///         !i.FullName.StartsWith("System") &&
   ///         !i.FullName.StartsWith("Microsoft") &&
   ///         !i.FullName.StartsWith("DotNetOpenAuth") &&
   ///         !i.FullName.StartsWith("WebMatrix")
   ///      ));
   ///   }
   ///
   ///}
   /// </example>
   public interface IAssemblyLoader
	{

      /// <summary>
      /// Loads the assemblies and returns the references to them.
      /// </summary>
      /// <returns></returns>
		IEnumerable<Assembly> LoadAssemblies();

	}
}
