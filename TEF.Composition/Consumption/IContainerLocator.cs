﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TEF.Consumption
{
	/// <summary>
	/// Gets the container from a specific location.  This is provided so the framework doesn't have to specify the location of the container, giving you the flexibility of storing it wherever you like.
	/// </summary>
	public interface IContainerLocator
	{

		/// <summary>
		/// Gets the reference to the container.
		/// </summary>
		/// <returns>The container instance.</returns>
		object GetContainer();

		/// <summary>
		/// Updates the container in the underlying store; run after all container references have been loaded.
		/// </summary>
		/// <param name="container">The container instance.</param>
		void UpdateContainer(object container);

	}
}
