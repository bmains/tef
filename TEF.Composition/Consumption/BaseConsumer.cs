﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TEF.Composition;


namespace TEF.Consumption
{
	/// <summary>
	/// This is a base class for consuming the references.  Because there are so many different types of DI container implementations, the idea here was to abstract the common tasks of receiving, and leave just the registration parts abstract.  That way, each container can perform the bare minimum and leave the rest to the framework.  If you don't want to use this base class, that is fine; simply creating a class that implements <see cref="IConsumer"/> is sufficient.
	/// </summary>
   /// <remarks>It would be possible to create extension classes to implement all of these base behaviors into a set of separate extension projects.  The difficulty in doing so is sometimes the pace at which a lot of these DI containers add additional features.  I placed examples online where you can find more information of practical examples using different DI containers, which allows me to not have to target different versions of each DI container.</remarks>
	public abstract class BaseConsumer : IConsumer
	{

		/// <summary>
		/// Consumes the given type mappings by creating the container and inserting the given types into the container.
		/// </summary>
		/// <param name="types">The types to being consumed; this process is the main driver for taking the list of registered types and adding them to the container.</param>
		/// <returns>The container (as a generic object reference; it can be cast to the appropriate container type) that was created via the <see cref="CreateContainer"/> method.</returns>
		public object Consume(IEnumerable<Composition.TypeMapping> types)
		{
			var container = this.CreateContainer();

			this.Consume(container, types);

			return container;
		}

      /// <summary>
      /// This method does the work of taking the container reference and custom list of types, and inserting those type mappings into the container.  This method can be overridden, but generally is not necessary.  This method defers to <see cref="RegisterCollection(object, Type, IEnumerable{Type}, string, object)"/> and <see cref="RegisterType(object, Type, Type, string, object)"/> methods to do the actual work of inserting the type(s) into the container.
      /// </summary>
      /// <param name="container">The generic form of the container.</param>
      /// <param name="types">The list of type mappings.</param>
		protected virtual void Consume(object container, IEnumerable<Composition.TypeMapping> types)
		{
         foreach (var typemap in types)
         {
            if (typemap.Implementations.Count > 1)
            {
               this.RegisterCollection(container, typemap.ContractType, typemap.Name, typemap.Implementations);
            }
            else if (typemap.Implementations.Count == 1)
            {
               var item = typemap.Implementations[0];
               this.RegisterType(container, typemap.ContractType, item.ImplementationType, typemap.Name, item.ScopeLifetime);
            }
         }


         
			//Can also group types, if you have more than one using the same signature, using something like:
			//var typeGroups = types.GroupBy(i => new { i.ContractType, i.Name });

			//foreach (var typeGroup in typeGroups)
			//{
			//	if (typeGroup.Count() > 1)
			//	{
			//		var scopes = typeGroup.SelectMany(i => i.LifetimeScope).ToList();
			//		if (scopes.Select(i => i.GetKey()).Distinct().Count() != 1)
			//			throw new InvalidOperationException("Cannot support multiple scopes for 1 registration for type: " + typeGroup.Key.ContractType.Name + (string.IsNullOrEmpty(typeGroup.Key.Name) ? "" : ", Named " + (typeGroup.Key.Name ?? "(Default)")));

			//		var convertedScope = ConvertLifeTimeScope(scopes[0]);
			//		this.RegisterCollection(container, typeGroup.Key.ContractType, typeGroup.Select(i => i.ImplementationType), typeGroup.Key.Name, convertedScope);
			//	}
			//	else
			//	{
			//		var typeReg = typeGroup.First();

			//		if (typeReg.LifetimeScope.Count != 1)
			//			throw new InvalidOperationException("Cannot support multiple scopes for 1 registration for type: " + typeReg.ContractType.Name + (string.IsNullOrEmpty(typeGroup.Key.Name) ? "" : ", Named " + (typeGroup.Key.Name ?? "(Default)")));

			//		var scope = typeReg.LifetimeScope[0];
			//		var lifecycle = ConvertLifeTimeScope(scope);

			//		if (lifecycle == null)
			//			throw new NotSupportedException("An invalid lifecycle scope was detected, which is not currently supported: " + scope.ToString());

			//		this.RegisterType(container, typeReg.ContractType, typeReg.ImplementationType, typeReg.Name, lifecycle);
			//	}
			//}
		}

      
		/// <summary>
		/// Creates a new container instance, which is used during the consumption time and then returned to the underlying framework.
		/// </summary>
		protected abstract object CreateContainer();

      /// <summary>
      /// Default implementation handles the assumption on what the lifestyle should be, with the lowest category winning.  Custom categories are not evaluated correctly in this routine.  This method is overridable.
      /// </summary>
      /// <param name="scopes">The scope.</param>
      /// <returns>The prioritized scope.</returns>
      protected abstract TypeImplementationScope HandleScopeMismatch(List<TypeImplementationScope> scopes);

		/// <summary>
		/// Registers a collection of implementations for a given contract/name pairs.
		/// </summary>
		/// <param name="container">The container instance.</param>
		/// <param name="contractType">The type of the contract.</param>
		/// <param name="implementations">The types/scopes that implement the contract.</param>
		protected void RegisterCollection(object container, Type contractType, string name, IEnumerable<TypeImplementationRegistration> implementations)
      {
         var impl = implementations.Select(i => i.ScopeLifetime).Distinct().ToList();
         TypeImplementationScope finalScope = null;
         if (impl.Count > 1)
            finalScope = this.HandleScopeMismatch(impl);
         else
            finalScope = impl[0];

         this.RegisterTypeCollection(container, contractType, name, implementations.Select(i => i.ImplementationType), finalScope);
      }

      /// <summary>
      /// Registers a contact/implementation type and name variation.
      /// </summary>
      /// <param name="container">The container.</param>
      /// <param name="contractType">The type of the contract.</param>
      /// <param name="implementationType">The type that implement the contract.</param>
      /// <param name="name">The name registration.</param>
      /// <param name="scopeLifecycle">The lifecycle to scope the object to.</param>
      protected abstract void RegisterType(object container, Type contractType, Type implementationType, string name, TypeImplementationScope scopeLifecycle);

      /// <summary>
      /// Registers a contract/collection of implementations, with name variation.
      /// </summary>
      /// <param name="container">The container.</param>
      /// <param name="contractType">The contract type.</param>
      /// <param name="name">The named parameter.</param>
      /// <param name="implementations">The implementations list.</param>
      /// <param name="scopeLifecycle">The lifecycle to scope the object to.</param>
      protected abstract void RegisterTypeCollection(object container, Type contractType, string name, IEnumerable<Type> implementations, TypeImplementationScope scopeLifecycle);

	}

}
