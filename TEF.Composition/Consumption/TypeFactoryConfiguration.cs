﻿using System;
using System.Collections.Generic;
using System.Linq;

using TEF.Composition;


namespace TEF.Consumption
{
	/// <summary>
	/// The configuration of the TypeFactory.
	/// </summary>
	public class TypeFactoryConfiguration
	{
      private IAssemblyLoader _assemblyLoader = null;
      private IConsumer _consumer = null;
      private IMetadataImporter _metadataImporter = null;




		/// <summary>
		/// Indicates the loader of assemblies, which is often the AppDomainAssemblyLoader in TEF.NET project; however, the framework can't be sure since it supports portable and TEF.NET project does not.
		/// </summary>
		/// <param name="loader">The loader of assemblies.</param>
		/// <returns>The configuration.</returns>
		/// <remarks>
		/// To load AppDomainAssemblyLoader, make sure the project where this is called refers to TEF.NET, or create a custom class that implements IAssemblyLoader.
		/// </remarks>
		public TypeFactoryConfiguration AssemblyLoader(IAssemblyLoader loader)
		{
			if (loader == null)
				throw new ArgumentNullException("loader");

			_assemblyLoader = loader;
			return this;
		}

		/// <summary>
		/// Indicates the consumer of type registrations.
		/// </summary>
		/// <param name="consumer">The consumer.</param>
		/// <returns></returns>
		public TypeFactoryConfiguration Consumer(IConsumer consumer)
		{
			if (consumer == null)
				throw new ArgumentNullException("consumer");

			_consumer = consumer;
			return this;
		}

		/// <summary>
		/// Indicates a custom importer of types.  This allows you to customize how the types are imported into the system, but is completely optional.
		/// </summary>
		/// <param name="metadata">The importer.</param>
		/// <returns>The configuration.</returns>
		/// <remarks>This is completely optional to set; it defaults to the existing implementation.</remarks>
		public TypeFactoryConfiguration MetadataImporter(IMetadataImporter metadata)
		{
			if (metadata == null)
				throw new ArgumentNullException("metadata");

         _metadataImporter = metadata;
			return this;
		}


      internal IAssemblyLoader GetAssemblyLoader()
      {
         return _assemblyLoader;
      }

      internal IConsumer GetConsumer()
      {
         return _consumer;
      }

      internal IMetadataImporter GetMetadataImporter()
      {
         return _metadataImporter;
      }

	}
}
