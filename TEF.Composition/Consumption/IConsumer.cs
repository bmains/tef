﻿using System;
using System.Collections.Generic;
using System.Linq;

using TEF.Composition;


namespace TEF.Consumption
{
	/// <summary>
	/// The base interface that represents the API for consuming an array of type mappings.  It's more common to inherit from the base class implementation, <see cref="BaseConsumer"/> instead, but this works too.
	/// </summary>
   /// <example>
   /// public class SimpleInjectorConsumer : IConsumer
   /// {
   ///		public object Consume(IEnumerable<TypeMapping> types)
   ///		{
   ///			var container = new Container();
   ///
   ///			//Can also group types, if you have more than one using the same signature, using something like:
   ///			var typeGroups = types.GroupBy(i => i.ContractType);
   ///			foreach (var typeGroup in typeGroups)
   ///			{
   ///				if (typeGroup.Count() > 1)
   ///				{
   ///					container.RegisterCollection(typeGroup.Key, typeGroup.Select(i => i.ImplementationType));
   ///				}
   ///				else
   ///					container.Register(typeGroup.Key, typeGroup.First().ImplementationType);
   ///			}
   ///
   ///			return container;
   ///		}
   ///
   ///		public object ConvertLifeTimeScope(LifeTimeScopeRegistration registration)
   ///		{
   ///			var scope = registration.Scope;
   ///
   ///			if (scope == LifeTimeScope.Singleton)
   ///				return Lifestyle.Singleton;
   ///			else if (scope == LifeTimeScope.Transient)
   ///				return Lifestyle.Transient;
   ///			else if (scope == LifeTimeScope.PerThread)
   ///				return Lifestyle.Scoped;
   ///			else if (scope == LifeTimeScope.PerRequest)
   ///				return new WebRequestLifestyle();
   ///			else
   ///				return null;
   ///		}
   /// }
   /// </example>
	public interface IConsumer
	{

		/// <summary>
		/// Consumes the types and loads them into the container.
		/// </summary>
		/// <param name="types">The types.</param>
		/// <returns>The container that was created.</returns>
		object Consume(IEnumerable<TypeMapping> types);
      
	}
}
