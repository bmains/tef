﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TEF.Consumption
{
   [TestClass]
   public class NonSystemAppDomainAssemblyLoaderTest
   {
      [TestMethod]
      public void LoadingAssembliesReturnsResults()
      {
         var obj = new NonSystemAppDomainAssemblyLoader();

         var assemblies = obj.LoadAssemblies().ToList();

         Assert.IsTrue(assemblies.Count > 0);
      }

      [TestMethod]
      public void LoadingAssembliesDoesNotHaveSystemNames()
      {
         var obj = new NonSystemAppDomainAssemblyLoader();

         var assemblies = obj.LoadAssemblies().ToList();

         Assert.IsTrue(assemblies.All(i => !i.FullName.StartsWith("System")));
         Assert.IsTrue(assemblies.All(i => !i.FullName.StartsWith("Microsoft")));
         Assert.IsTrue(assemblies.All(i => !i.FullName.StartsWith("WebMatrix")));
         Assert.IsTrue(assemblies.All(i => !i.FullName.StartsWith("DotNetOpenAuth")));
      }

   }
}
