﻿using System;
using System.Linq;
using System.Collections.Generic;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TEF.Composition;


namespace TEF.Consumption
{
   [TestClass]
   public class TypeFactoryConfigurationTest
   {

      [TestMethod]
      public void CreatingAssemblyLoaderAssignsOK()
      {
         var config = new TypeFactoryConfiguration();
         var mock = new Mock<IAssemblyLoader>();
         
         config.AssemblyLoader(mock.Object);
      }

      [TestMethod]
      public void CreatingConsumerAssignsOK()
      {
         var config = new TypeFactoryConfiguration();
         var mock = new Mock<IConsumer>();

         config.Consumer(mock.Object);
      }

      [TestMethod]
      public void CreatingMetadataImporterAssignsOK()
      {
         var config = new TypeFactoryConfiguration();
         var mock = new Mock<IMetadataImporter>();

         config.MetadataImporter(mock.Object);
      }


      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void SettingNullAssemblyLoaderThrowsException()
      {
         var config = new TypeFactoryConfiguration();
         config.AssemblyLoader(null);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void SettingNullConsumerThrowsException()
      {
         var config = new TypeFactoryConfiguration();
         config.Consumer(null);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void SettingNullMetadataImporterThrowsException()
      {
         var config = new TypeFactoryConfiguration();
         config.MetadataImporter(null);
      }

   }
}
