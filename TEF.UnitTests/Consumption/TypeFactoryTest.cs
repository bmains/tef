﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TEF.Composition;


namespace TEF.Consumption
{
   [TestClass]
   public class TypeFactoryTest
   {
      protected class TypeFactoryMock : TypeFactory
      {

         public IAssemblyLoader Loader
         {
            get { return _assemblyLoader; }
            set { _assemblyLoader = value; }
         }

         public IConsumer Consumer
         {
            get { return _consumer; }
            set { _consumer = value; }
         }

         public IMetadataImporter MetadataImporter
         {
            get { return _metdataImporter; }
            set { _metdataImporter = value; }
         }



         public TypeFactoryMock() : base() { }
       
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void CreatingWithNoAssemblyLoaderThrowsException()
      {
         TypeFactory.Configure((c) =>
         {
            c.Consumer(new Mock<IConsumer>().Object).MetadataImporter(new Mock<IMetadataImporter>().Object);
         });
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void CreatingWithNoConsumerThrowsException()
      {
         TypeFactory.Configure((c) =>
         {
            c.AssemblyLoader(new Mock<IAssemblyLoader>().Object).MetadataImporter(new Mock<IMetadataImporter>().Object);
         });
      }

      [TestMethod]
      public void CreatingWithNoMetadataImporterDefaultsAndThrowsNoException()
      {
         TypeFactory.Configure((c) =>
         {
            c.AssemblyLoader(new Mock<IAssemblyLoader>().Object).Consumer(new Mock<IConsumer>().Object);
         });
      }

      [TestMethod]
      public void InitializingContainerCreatesReferencesOK()
      {
         var assembly = Assembly.GetExecutingAssembly();
         var loader = new Mock<IAssemblyLoader>();
         loader.Setup(i => i.LoadAssemblies()).Returns(new Assembly[] { assembly });
         
         var importer = new Mock<IMetadataImporter>();
         importer.Setup(i => i.GetAssemblyTypes(It.IsAny<IEnumerable<Assembly>>())).Returns(new TypeMapping[] 
         {
            new TypeMapping
            {
               ContractType = typeof(IMockService),
               Implementations = new List<TypeImplementationRegistration> { new TypeImplementationRegistration { ImplementationType = typeof(MockService) } }
            }
         });
         importer.Setup(i => i.GetClassTypes(It.IsAny<IEnumerable<Assembly>>())).Returns(new TypeMapping[]
         {
            new TypeMapping
            {
               ContractType = typeof(IMockService2),
               Implementations = new List<TypeImplementationRegistration> { new TypeImplementationRegistration { ImplementationType = typeof(MockService2) } }
            }
         });

         var container = new object();
         var consumer = new Mock<IConsumer>();
         consumer.Setup(i => i.Consume(It.IsAny<List<TypeMapping>>())).Returns(container);
         
         var factory = new TypeFactoryMock();
         factory.Consumer = consumer.Object;
         factory.Loader = loader.Object;
         factory.MetadataImporter = importer.Object;
         
         var result = factory.Initialize();

         Assert.AreEqual(container, result);
      }

      [TestMethod]
      public void PostInitializingContainerWorksOK()
      {
         var assembly = Assembly.GetExecutingAssembly();
         var loader = new Mock<IAssemblyLoader>();
         loader.Setup(i => i.LoadAssemblies()).Returns(new Assembly[] { assembly });

         var importer = new Mock<IMetadataImporter>();
         importer.Setup(i => i.GetAssemblyTypes(It.IsAny<IEnumerable<Assembly>>())).Returns(new TypeMapping[]
         {
            new TypeMapping
            {
               ContractType = typeof(IMockService),
               Implementations = new List<TypeImplementationRegistration> { new TypeImplementationRegistration { ImplementationType = typeof(MockService) } }
            }
         });
         importer.Setup(i => i.GetClassTypes(It.IsAny<IEnumerable<Assembly>>())).Returns(new TypeMapping[]
         {
            new TypeMapping
            {
               ContractType = typeof(IMockService2),
               Implementations = new List<TypeImplementationRegistration> { new TypeImplementationRegistration { ImplementationType = typeof(MockService2) } }
            }
         });

         //List only used for verifying test
         var container = new List<object>();
         var consumer = new Mock<IConsumer>();
         consumer.Setup(i => i.Consume(It.IsAny<List<TypeMapping>>())).Returns(container);

         var factory = new TypeFactoryMock();
         factory.Consumer = consumer.Object;
         factory.Loader = loader.Object;
         factory.MetadataImporter = importer.Object;
         
         //I would never use a list here but just to test this out
         //without requiring a new stub, a list seemed like a good quick
         //testing solution here
         var result = factory.Initialize((o) =>
         {
            ((List<object>)o).Add(new object());
         });

         Assert.AreEqual(container, result);
         Assert.AreEqual(1, container.Count);
      }

   }
}
