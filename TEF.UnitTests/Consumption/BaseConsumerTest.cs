﻿using System;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using TEF.Composition;


namespace TEF.Consumption
{
   [TestClass]
   public class BaseConsumerTest
   {


      [TestMethod]
      public void CreatingContainerInvokesCorrectMethods()
      {
         var container = new object();
         IEnumerable<TypeMapping> mapping = new List<TypeMapping>();

         var mock = new Mock<BaseConsumer> { CallBase = true };
         mock.Protected().Setup<object>("CreateContainer").Returns(container);
         mock.Protected().Setup("Consume", container, mapping).Verifiable();

         var result = mock.Object.Consume(mapping);

         Assert.AreEqual(container, result);
         Mock.Verify(mock);
      }

      [TestMethod]
      public void RegisteringCollectionWithSingleScopeHandlesOK()
      {
         var scope = new TypeImplementationScope(LifeTimeScope.Singleton);
         IEnumerable<TypeImplementationRegistration> registrations = new List<TypeImplementationRegistration>
         {
            new TypeImplementationRegistration { ImplementationType = typeof(MockService), ScopeLifetime = scope },
            new TypeImplementationRegistration { ImplementationType = typeof(MockService2), ScopeLifetime = new TypeImplementationScope(LifeTimeScope.Singleton) }
         };
         var container = new object();
         Type contractType = typeof(IMockService);
         string name = null;
         IEnumerable<Type> implementations = new Type[] { typeof(MockService), typeof(MockService2) };

         var mock = new Mock<BaseConsumer>();
         mock.Protected().Setup("HandleScopeMismatch", It.IsAny<List<TypeImplementationScope>>()).Throws(new AssertFailedException());
         mock.Protected().Setup("RegisterTypeCollection", container, contractType, ItExpr.IsNull<string>(), implementations, scope).Verifiable();
         
         var obj = mock.Object;

         obj.GetType().GetMethod("RegisterCollection", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(obj, new object[] { container, contractType, name, registrations });

         Mock.Verify(mock);
      }
     
   }
}
