﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TEF;
using Moq;
using Moq.Protected;


namespace TEF.Composition
{
   [TestClass]
   public class TEFMetadataImporterTest
   {

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void ExceptionWhenNullAssemblyArgument()
      {
         var importer = new Mock<TEFMetadataImporter>() { CallBase = true };
         Assembly assembly = null;

         importer.Object.GetAssemblyTypes(assembly);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void ExceptionWhenNullAssemblyTypesArgument()
      {
         var importer = new Mock<TEFMetadataImporter>() { CallBase = true };
         Assembly[] types = null;

         importer.Object.GetAssemblyTypes(types);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void ExceptionWhenNullClassTypesAssemblyArgument()
      {
         var importer = new Mock<TEFMetadataImporter>() { CallBase = true };
         Assembly assembly = null;

         importer.Object.GetClassTypes(assembly);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void ExceptionWhenNullClassTypesArgument()
      {
         var importer = new Mock<TEFMetadataImporter>() { CallBase = true };
         Type[] types = null;

         importer.Object.GetClassTypes(types);
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void TypeLoadException_GettingClassTYpesThrowsReflectionTypeLoadException()
      {
         IEnumerable<Type> types = new Type[] { typeof(MockService) };

         var importer = new Mock<TEFMetadataImporter> { CallBase = true };
         importer.Protected().Setup<IEnumerable<TypeMapping>>("ParseTypeForAttribute", types).Throws(new System.Reflection.ReflectionTypeLoadException(new Type[] { typeof(MockService) }, new Exception[] { new Exception("LOAD EXCEPTION") }));
         
         importer.Object.GetClassTypes(types);
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void TypeLoadException_GettingClassTypesFromAssemblyThrowsReflectionTypeLoadException()
      {
         Assembly dll = typeof(MockService).Assembly;

         var importer = new Mock<TEFMetadataImporter> { CallBase = true };
         importer.Protected().Setup<IEnumerable<TypeMapping>>("ParseTypeForAttribute", dll.GetTypes().AsEnumerable()).Throws(new System.Reflection.ReflectionTypeLoadException(new Type[] { typeof(MockService) }, new Exception[] { new Exception("LOAD EXCEPTION") }));

         importer.Object.GetClassTypes(dll);
      }


      [TestMethod]
      public void GetAssemblyTypesWorksOK_Assemblies()
      {
         var mockDep = new Mock<IExportDependencyFinder>();
         var mockTypes = new Mock<IExportTypeFinder>();
         var list = new List<TypeMapping>();
         list.Add(new TypeMapping { ContractType = typeof(IMockService), Implementations = GetDefaultImplementationList(typeof(MockService)) });
         list.Add(new TypeMapping { ContractType = typeof(IMockService2), Implementations = GetDefaultImplementationList(typeof(MockService2)) });

         mockDep.Setup(i => i.GetAssemblyTypes(It.IsAny<Assembly>())).Returns(list);

         var tef = new TEFMetadataImporter(mockDep.Object, mockTypes.Object);
         var types = tef.GetAssemblyTypes(new Assembly[] { typeof(TEFMetadataImporterTest).Assembly, typeof(Moq.Mock).Assembly }).ToList();

         Assert.AreEqual(4, types.Count);
         Assert.AreEqual(typeof(IMockService), types[0].ContractType);
         Assert.AreEqual(typeof(IMockService2), types[1].ContractType);
      }

      [TestMethod]
      public void GetAssemblyTypesWorksOK_Assembly()
      {
         var mockDep = new Mock<IExportDependencyFinder>();
         var mockTypes = new Mock<IExportTypeFinder>();
         var list = new List<TypeMapping>();
         list.Add(new TypeMapping { ContractType = typeof(IMockService), Implementations = GetDefaultImplementationList(typeof(MockService)) });
         list.Add(new TypeMapping { ContractType = typeof(IMockService2), Implementations = GetDefaultImplementationList(typeof(MockService2)) });

         mockDep.Setup(i => i.GetAssemblyTypes(It.IsAny<Assembly>())).Returns(list);

         var tef = new TEFMetadataImporter(mockDep.Object, mockTypes.Object);
         var types = tef.GetAssemblyTypes(typeof(TEFMetadataImporterTest).Assembly).ToList();

         Assert.AreEqual(2, types.Count);
         Assert.AreEqual(typeof(IMockService), types[0].ContractType);
         Assert.AreEqual(typeof(IMockService2), types[1].ContractType);
      }

      [TestMethod]
      public void GetClassTypesWorksOK_Assemblies()
      {
         var mockDep = new Mock<IExportDependencyFinder>();
         var mockTypes = new Mock<IExportTypeFinder>();
         var list = new List<TypeMapping>();
         list.Add(new TypeMapping { ContractType = typeof(IMockService), Implementations = GetDefaultImplementationList(typeof(MockService)) });
         list.Add(new TypeMapping { ContractType = typeof(IMockService2), Implementations = GetDefaultImplementationList(typeof(MockService2)) });

         mockTypes.Setup(i => i.GetClassTypes(It.IsAny<IEnumerable<Type>>())).Returns(list);

         var tef = new TEFMetadataImporter(mockDep.Object, mockTypes.Object);
         var types = tef.GetClassTypes(new Assembly[] { typeof(TEFMetadataImporterTest).Assembly, typeof(Moq.Mock).Assembly }).ToList();

         Assert.AreEqual(4, types.Count);
         Assert.AreEqual(typeof(IMockService), types[0].ContractType);
         Assert.AreEqual(typeof(IMockService2), types[1].ContractType);
      }

      [TestMethod]
      public void GetClassTypesWorksOK_Assembly()
      {
         var mockDep = new Mock<IExportDependencyFinder>();
         var mockTypes = new Mock<IExportTypeFinder>();
         var list = new List<TypeMapping>();
         list.Add(new TypeMapping { ContractType = typeof(IMockService), Implementations = GetDefaultImplementationList(typeof(MockService)) });
         list.Add(new TypeMapping { ContractType = typeof(IMockService2), Implementations = GetDefaultImplementationList(typeof(MockService2)) });

         mockTypes.Setup(i => i.GetClassTypes(It.IsAny<IEnumerable<Type>>())).Returns(list);

         var tef = new TEFMetadataImporter(mockDep.Object, mockTypes.Object);
         var types = tef.GetClassTypes(typeof(TEFMetadataImporterTest).Assembly).ToList();

         Assert.AreEqual(2, types.Count);
         Assert.AreEqual(typeof(IMockService), types[0].ContractType);
         Assert.AreEqual(typeof(IMockService2), types[1].ContractType);
      }

      [TestMethod]
      public void GetClassTypesWorksOK_List()
      {
         var mockDep = new Mock<IExportDependencyFinder>();
         var mockTypes = new Mock<IExportTypeFinder>();
         var list = new List<TypeMapping>();
         list.Add(new TypeMapping { ContractType = typeof(IMockService), Implementations = GetDefaultImplementationList(typeof(MockService)) });
         list.Add(new TypeMapping { ContractType = typeof(IMockService2), Implementations = GetDefaultImplementationList(typeof(MockService2)) });

         mockTypes.Setup(i => i.GetClassTypes(It.IsAny<IEnumerable<Type>>())).Returns(list);

         var tef = new TEFMetadataImporter(mockDep.Object, mockTypes.Object);
         var types = tef.GetClassTypes(new Type[] { typeof(MockService), typeof(MockService2) }).ToList();

         Assert.AreEqual(2, types.Count);
         Assert.AreEqual(typeof(IMockService), types[0].ContractType);
         Assert.AreEqual(typeof(IMockService2), types[1].ContractType);
      }

      private List<TypeImplementationRegistration> GetDefaultImplementationList(Type type)
      {
         return new List<TypeImplementationRegistration> { new TypeImplementationRegistration { ImplementationType = type } };
      }

   }
}
