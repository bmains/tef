﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;


namespace TEF.Composition
{
   [TestClass]
   public class TypeMappingTest
   {

      [TestMethod]
      public void GettingAndSettingPropertiesWorksOK()
      {
         var map = new TypeMapping();
         var scope = new Mock<TypeImplementationScope>().Object;

         map.ContractType = typeof(IMockService);
         map.Implementations.Add(new TypeImplementationRegistration
         {
            ImplementationType = typeof(MockService),
            ScopeLifetime = scope
         });
         map.Name = "XYZ";

         Assert.AreEqual(typeof(IMockService), map.ContractType);
         Assert.AreEqual(1, map.Implementations.Count);
         Assert.AreEqual(typeof(MockService), map.Implementations[0].ImplementationType);
         Assert.AreEqual(scope, map.Implementations[0].ScopeLifetime);
         Assert.AreEqual("XYZ", map.Name);
      }
   }
}
