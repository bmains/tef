﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEF.Composition
{
   [TestClass]
   public class ExportTypeAttributeTest
   {

      [TestMethod]
      public void CreatingWorksOK_Contract()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService));

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.Singleton, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_ContractName()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService), "NAMED");

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.Singleton, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_ContractNameScope()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService), "NAMED", LifeTimeScope.PerThread);

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.PerThread, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_ContractNameScopeAsCustom()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService), "NAMED", LifeTimeScope.Custom, "TEST");

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.Custom, attrib.LifeTimeScope);
         Assert.AreEqual("TEST", attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_ContractScope()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService), LifeTimeScope.PerThread);

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.PerThread, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_ContractScopeAsCustom()
      {
         var attrib = new ExportTypeAttribute(typeof(IMockService), LifeTimeScope.Custom, "TEST");

         Assert.AreEqual(attrib.ContractType, typeof(IMockService));
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.Custom, attrib.LifeTimeScope);
         Assert.AreEqual("TEST", attrib.LifeTimeScopeCustomName);
      }

   }
}
