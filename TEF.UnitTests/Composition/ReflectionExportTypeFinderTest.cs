﻿using System;
using System.Linq;
using System.Collections.Generic;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TEF.Composition
{
   [TestClass]
   public class ReflectionExportTypeFinderTest
   {

      [TestMethod]
      public void DuplicatedMatchesReturnsSingleResultset()
      {
         var implType = typeof(MockService);
         var implType2 = typeof(MockService2);
         var contractType = typeof(IMockService);

         var obj = new Mock<ReflectionExportTypeFinder>() { CallBase = true };
         obj.Setup(i => i.GetAttributeForType(implType)).Returns(() => new ExportTypeAttribute(contractType));

         var results = obj.Object.GetClassTypes(new Type[] { implType, implType2 }).ToList();

         Assert.AreEqual(1, results.Count);
      }

      [TestMethod]
      public void GettingReflectedObjectsFindsNothing()
      {
         var implType = typeof(MockService);
         var contractType = typeof(IMockService);

         var obj = new Mock<ReflectionExportTypeFinder>() { CallBase = true };
         obj.Setup(i => i.GetAttributeForType(implType)).Returns(() => null);

         var results = obj.Object.GetClassTypes(new Type[] { implType }).ToList();

         Assert.AreEqual(0, results.Count);
      }

      [TestMethod]
      public void GettingReflectedObjectsWorksOK()
      {
         var implType = typeof(MockService);
         var contractType = typeof(IMockService);

         var obj = new Mock<ReflectionExportTypeFinder>() { CallBase = true };
         obj.Setup(i => i.GetAttributeForType(implType)).Returns(new ExportTypeAttribute(contractType));

         var results = obj.Object.GetClassTypes(new Type[] { implType }).ToList();

         Assert.AreEqual(1, results.Count);
         Assert.AreEqual(1, results[0].Implementations.Count);
         Assert.AreEqual(implType, results[0].Implementations[0].ImplementationType);
         Assert.AreEqual(contractType, results[0].ContractType);
      }

      [TestMethod]
      public void HasAttributeCheckReturnsObject()
      {
         var obj = new ReflectionExportTypeFinder();

         var result = obj.GetAttributeForType(typeof(ExportTypeService));

         Assert.IsNotNull(result);
      }

      [TestMethod]
      public void HasAttributeCheckReturnsNull()
      {
         var obj = new ReflectionExportTypeFinder();

         var result = obj.GetAttributeForType(typeof(NonMockService));

         Assert.IsNull(result);
      }

   }
}
