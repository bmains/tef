﻿using System;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace TEF.Composition
{
   [TestClass]
   public class MetadataImporterExtensionsTest
   {
      [TestMethod]
      public void GettingAssemblyTypesFromAppDomainWorksOK()
      {
         var importer = new Mock<IMetadataImporter>();
         importer.Setup(m => m.GetAssemblyTypes(It.IsAny<Assembly>())).Verifiable();

         importer.Object.GetAssemblyTypes(AppDomain.CurrentDomain);

         Mock.Verify(importer);
      }

      [TestMethod]
      public void GettingClassTypesFromAppDomainWorksOK()
      {
         var importer = new Mock<IMetadataImporter>();
         importer.Setup(m => m.GetClassTypes(It.IsAny<Assembly>())).Verifiable();

         importer.Object.GetClassTypes(AppDomain.CurrentDomain);

         Mock.Verify(importer);
      }
   }
}
