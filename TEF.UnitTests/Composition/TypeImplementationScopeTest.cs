﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TEF.Composition
{
   [TestClass]
   public class TypeImplementationScopeTest
   {
      [TestMethod]
      [ExpectedException(typeof(ArgumentException))]
      public void CreatingWithInvalidScopeFails()
      {
         var scope = new TypeImplementationScope(LifeTimeScope.Custom);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void CreatingWithInvalidScopeNameAsEmptyFails()
      {
         new TypeImplementationScope(string.Empty);
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentNullException))]
      public void CreatingWithInvalidScopeNameAsNullFails()
      {
         new TypeImplementationScope((string)null);
      }

      [TestMethod]
      public void CreatingWithValidScopeWorksOK()
      {
         var scope = new TypeImplementationScope(LifeTimeScope.Transient);

         Assert.AreEqual(LifeTimeScope.Transient, scope.Scope);
         Assert.IsNull(scope.CustomScopeName);
      }

      [TestMethod]
      public void CreatingWithValidScopeNameWorksOK()
      {
         var scope = new TypeImplementationScope("ABC");

         Assert.AreEqual(LifeTimeScope.Custom, scope.Scope);
         Assert.AreEqual("ABC", scope.CustomScopeName);
      }

      [TestMethod]
      public void CheckingEqualityMatches()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = new TypeImplementationScope(LifeTimeScope.Transient);

         Assert.IsTrue(s1.Equals(s2));
      }

      [TestMethod]
      public void CheckingEqualityDoesntMatch()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = new TypeImplementationScope(LifeTimeScope.Singleton);

         Assert.IsFalse(s1.Equals(s2));
      }

      [TestMethod]
      public void CheckingCustomEqualityMatches()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABC");

         Assert.IsTrue(s1.Equals(s2));
      }

      [TestMethod]
      public void CheckingCustomEqualityDoesntMatch()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABD");

         Assert.IsFalse(s1.Equals(s2));
      }

      [TestMethod]
      public void CheckingEqualityOperatorMatches()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = new TypeImplementationScope(LifeTimeScope.Transient);

         Assert.IsTrue(s1 == s2);
      }

      [TestMethod]
      public void CheckingEqualityOperatorDoesntMatch()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = new TypeImplementationScope(LifeTimeScope.Singleton);

         Assert.IsFalse(s1 == s2);
      }

      [TestMethod]
      public void CheckingCustomEqualityOperatorMatches()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABC");

         Assert.IsTrue(s1 == s2);
      }

      [TestMethod]
      public void CheckingCustomEqualityOperatorDoesntMatch()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABD");

         Assert.IsFalse(s1 == s2);
      }

      [TestMethod]
      public void CheckingInequalityOperatorMatches()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.PerRequest);
         var s2 = new TypeImplementationScope(LifeTimeScope.Transient);

         Assert.IsTrue(s1 != s2);
      }

      [TestMethod]
      public void CheckingInequalityOperatorDoesntMatch()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Singleton);
         var s2 = new TypeImplementationScope(LifeTimeScope.Singleton);

         Assert.IsFalse(s1 != s2);
      }

      [TestMethod]
      public void CheckingCustomInequalityOperatorMatches()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABD");

         Assert.IsTrue(s1 != s2);
      }

      [TestMethod]
      public void CheckingCustomInequalityOperatorDoesntMatch()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = new TypeImplementationScope("ABC");

         Assert.IsFalse(s1 != s2);
      }

      [TestMethod]
      public void CheckingEqualityOperatorMatchesAgainstString()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = "Transient";

         Assert.IsTrue(s1 == s2);
      }

      [TestMethod]
      public void CheckingEqualityOperatorDoesntMatchAgainstString()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Transient);
         var s2 = "Singleton";

         Assert.IsFalse(s1 == s2);
      }

      [TestMethod]
      public void CheckingCustomEqualityOperatorMatchesAgainstString()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABC";

         Assert.IsTrue(s1 == s2);
      }

      [TestMethod]
      public void CheckingCustomEqualityOperatorDoesntMatchAgainstString()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABD";

         Assert.IsFalse(s1 == s2);
      }

      [TestMethod]
      public void CheckingInequalityOperatorMatchesAgainstString()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.PerRequest);
         var s2 = "Transient";

         Assert.IsTrue(s1 != s2);
      }

      [TestMethod]
      public void CheckingInequalityOperatorDoesntMatchAgainstString()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Singleton);
         var s2 = "Singleton";

         Assert.IsFalse(s1 != s2);
      }

      [TestMethod]
      public void CheckingCustomInequalityOperatorMatchesAgainstString()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABD";

         Assert.IsTrue(s1 != s2);
      }

      [TestMethod]
      public void CheckingCustomInequalityOperatorDoesntMatchAgainstString()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABC";

         Assert.IsFalse(s1 != s2);
      }

      [TestMethod]
      public void ComparingHashKeyScopeWorksOK()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Singleton);
         var s2 = "Singleton";

         Assert.IsTrue(s1.GetHashCode() == s2.GetHashCode());
      }

      [TestMethod]
      public void ComparingHashKeyScopeDoesntMatch()
      {
         var s1 = new TypeImplementationScope(LifeTimeScope.Singleton);
         var s2 = "PerRequest";

         Assert.IsFalse(s1.GetHashCode() == s2.GetHashCode());
      }

      [TestMethod]
      public void ComparingHashKeyCustomScopeWorksOK()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABC";

         Assert.IsTrue(s1.GetHashCode() == s2.GetHashCode());
      }

      [TestMethod]
      public void ComparingHashKeyCustomScopeDoesntMatch()
      {
         var s1 = new TypeImplementationScope("ABC");
         var s2 = "Custom_ABD";

         Assert.IsFalse(s1.GetHashCode() == s2.GetHashCode());
      }

      [TestMethod]
      public void GetScopeKey_HandlingCustomScopeReturnsCorrectKey()
      {
         var map = new TypeImplementationScope("A");
         
         Assert.AreEqual("Custom_A", map.GetScopeKey());
      }

      [TestMethod]
      public void GetScopeKey_HandlingNormalScopeWorksOK()
      {
         var map = new TypeImplementationScope(LifeTimeScope.PerRequest);
         
         Assert.AreEqual("PerRequest", map.GetScopeKey());
      }

   }
}
