﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace TEF.Composition
{
   [TestClass]
   public class TypeImplementationRegistrationTest
   {

      [TestMethod]
      public void GettingAndSettingPropertiesWorksOK()
      {
         var scope = new Mock<TypeImplementationScope>().Object;

         var map = new TypeImplementationRegistration();
         map.ImplementationType = typeof(TypeImplementationRegistration);
         map.ScopeLifetime = scope;

         Assert.AreEqual(typeof(TypeImplementationRegistration), map.ImplementationType);
         Assert.AreEqual(scope, map.ScopeLifetime);
      }
      
   }
}
