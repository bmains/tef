﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TEF.Composition
{
   [TestClass]
   public class ReflectionExportDependencyFinderTest
   {
      
      [TestMethod]
      public void FindingAttributesReturnsMatch()
      {
         var conType = typeof(IExportDependencyService);
         var obj = new ReflectionExportDependencyFinder();

         var attribs = obj.GetAttributes(conType.Assembly);

         Assert.IsTrue(attribs.Any(i => i.ContractType == conType));
      }

      [TestMethod]
      public void GettingCustomAttributesReturnsEmpty()
      {
         var conType = typeof(IMockService);
         var implType = typeof(MockService);
         var obj = new Mock<ReflectionExportDependencyFinder>() { CallBase = true };
         obj.Setup(i => i.GetAttributes(It.IsAny<Assembly>())).Returns(new List<ExportDependencyAttribute>
         {
         });

         var results = obj.Object.GetAssemblyTypes(conType.Assembly).ToList();

         Assert.AreEqual(0, results.Count);
      }

      [TestMethod]
      public void GettingCustomAttributesReturnsResults()
      {
         var conType = typeof(IMockService);
         var implType = typeof(MockService);
         var obj = new Mock<ReflectionExportDependencyFinder>() { CallBase = true };
         obj.Setup(i => i.GetAttributes(It.IsAny<Assembly>())).Returns(new List<ExportDependencyAttribute>
         {
            new ExportDependencyAttribute(conType, implType)
         });

         var results = obj.Object.GetAssemblyTypes(conType.Assembly).ToList();

         Assert.AreEqual(1, results.Count);
         Assert.AreEqual(conType, results[0].ContractType);
         Assert.AreEqual(1, results[0].Implementations.Count);
         Assert.AreEqual(implType, results[0].Implementations[0].ImplementationType);
      }

      [TestMethod]
      public void GetImplementationsReturnsSingleCustomMatching()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.Custom, "ABC");
         var dep2 = new ExportDependencyAttribute(typeof(IMockService2), typeof(MockService2), LifeTimeScope.Custom, "ABC");
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(1, results.Count);
         Assert.AreEqual(typeof(MockService), results[0].ImplementationType);
         Assert.AreEqual(LifeTimeScope.Custom, results[0].ScopeLifetime.Scope);
         Assert.AreEqual("ABC", results[0].ScopeLifetime.CustomScopeName);
      }

      [TestMethod]
      public void GetImplementationsReturnsSingleStandardMatching()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.PerThread);
         var dep2 = new ExportDependencyAttribute(typeof(IMockService2), typeof(MockService2), LifeTimeScope.PerThread);
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(1, results.Count);
         Assert.AreEqual(typeof(MockService), results[0].ImplementationType);
         Assert.AreEqual(LifeTimeScope.PerThread, results[0].ScopeLifetime.Scope);
         Assert.AreEqual(null, results[0].ScopeLifetime.CustomScopeName);
      }

      [TestMethod]
      public void MultiImplementationsReturnsCorrectResultSet()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.PerThread);
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), LifeTimeScope.PerThread);
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(2, results.Count);
         Assert.AreEqual(typeof(MockService), results[0].ImplementationType);
         Assert.AreEqual(typeof(MockService2), results[1].ImplementationType);
         Assert.AreEqual(LifeTimeScope.PerThread, results[0].ScopeLifetime.Scope);
         Assert.AreEqual(LifeTimeScope.PerThread, results[1].ScopeLifetime.Scope);
         Assert.AreEqual(null, results[0].ScopeLifetime.CustomScopeName);
         Assert.AreEqual(null, results[1].ScopeLifetime.CustomScopeName);
      }

      [TestMethod]
      public void MultiImplementationsReturnsCorrectResultSetWithNamedScope()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAME", LifeTimeScope.PerThread);
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), "NAME", LifeTimeScope.PerThread);
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), "NAME", new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(2, results.Count);
         Assert.AreEqual(typeof(MockService), results[0].ImplementationType);
         Assert.AreEqual(typeof(MockService2), results[1].ImplementationType);
         Assert.AreEqual(LifeTimeScope.PerThread, results[0].ScopeLifetime.Scope);
         Assert.AreEqual(LifeTimeScope.PerThread, results[1].ScopeLifetime.Scope);
         Assert.AreEqual(null, results[0].ScopeLifetime.CustomScopeName);
         Assert.AreEqual(null, results[1].ScopeLifetime.CustomScopeName);
      }

      [TestMethod]
      public void MultiImplementationsDontReturnResultSetWithNamedScope()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAME", LifeTimeScope.PerThread);
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), "NAME", LifeTimeScope.PerThread);
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(0, results.Count);
      }

      [TestMethod]
      public void MultiImplementationsReturnsCorrectResultSetWithCustomLifetimeScope()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.Custom, "A");
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), LifeTimeScope.Custom, "A");
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(2, results.Count);
         Assert.AreEqual(typeof(MockService), results[0].ImplementationType);
         Assert.AreEqual(typeof(MockService2), results[1].ImplementationType);
         Assert.AreEqual(LifeTimeScope.Custom, results[0].ScopeLifetime.Scope);
         Assert.AreEqual(LifeTimeScope.Custom, results[1].ScopeLifetime.Scope);
         Assert.AreEqual("A", results[0].ScopeLifetime.CustomScopeName);
         Assert.AreEqual("A", results[1].ScopeLifetime.CustomScopeName);
      }

      [TestMethod]
      public void MultiImplementationsDontReturnResultSetWithCustomLifetimeScope1()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAMED", LifeTimeScope.Custom, "A");
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), "NAMED", LifeTimeScope.Custom, "A");
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), null, new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(0, results.Count);
      }

      [TestMethod]
      public void MultiImplementationsDontReturnResultSetWithCustomLifetimeScope2()
      {
         var dep = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.Custom, "A");
         var dep2 = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService2), LifeTimeScope.Custom, "A");
         var finder = new ReflectionExportDependencyFinder();

         var results = finder.GetImplementations(typeof(IMockService), "NAME", new List<ExportDependencyAttribute> { dep, dep2 });

         Assert.AreEqual(0, results.Count);
      }

   }
}
