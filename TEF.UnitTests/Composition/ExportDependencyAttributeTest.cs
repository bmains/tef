﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TEF.Composition
{
   [TestClass]
   public class ExportDependencyAttributeTest
   {
      [TestMethod]
      public void CreatingWorksOK_Types()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService));

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.Singleton, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_TypesName()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAMED");

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.Singleton, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_TypesNameScope()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAMED", LifeTimeScope.PerThread);

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.PerThread, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_TypesNameScopeSetToCustom()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), "NAMED", LifeTimeScope.Custom, "TEST");

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.AreEqual("NAMED", attrib.Name);
         Assert.AreEqual(LifeTimeScope.Custom, attrib.LifeTimeScope);
         Assert.AreEqual("TEST", attrib.LifeTimeScopeCustomName);
      }


      [TestMethod]
      public void CreatingWorksOK_TypesScope()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.PerRequest);

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.PerRequest, attrib.LifeTimeScope);
         Assert.IsNull(attrib.LifeTimeScopeCustomName);
      }

      [TestMethod]
      public void CreatingWorksOK_TypesScopeSetToCustom()
      {
         var attrib = new ExportDependencyAttribute(typeof(IMockService), typeof(MockService), LifeTimeScope.Custom, "TEST");

         Assert.AreEqual(typeof(IMockService), attrib.ContractType);
         Assert.AreEqual(typeof(MockService), attrib.ImplementationType);
         Assert.IsNull(attrib.Name);
         Assert.AreEqual(LifeTimeScope.Custom, attrib.LifeTimeScope);
         Assert.AreEqual("TEST", attrib.LifeTimeScopeCustomName);
      }

   }
}
