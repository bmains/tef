﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly:TEF.Composition.ExportDependency(typeof(TEF.IExportDependencyService), typeof(TEF.ExportDependencyService))]

namespace TEF
{
   public interface IMockService
   {
   }

   public class MockService : IMockService
   {
   }

   public interface IMockService2
   {
   }

   public class MockService2 : IMockService2
   {
   }

   public class NonMockService
   {
   }

   public interface IExportTypeService
   {

   }

   [TEF.Composition.ExportType(typeof(IExportTypeService))]
   public class ExportTypeService : IExportTypeService
   {

   }

   public interface IExportDependencyService
   {

   }

   public class ExportDependencyService : IExportDependencyService
   {

   }
}
