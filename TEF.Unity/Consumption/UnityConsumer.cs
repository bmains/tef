﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;

using TEF.Composition;


namespace TEF.Consumption
{
	public class UnityConsumer : IConsumer
	{
	

		public object Consume(IEnumerable<TypeMapping> types)
		{
			var container = new UnityContainer();

			//Can also group types, if you have more than one using the same signature, using something like:
			var typeGroups = types.GroupBy(i => i.ContractType);
			foreach (var typeGroup in typeGroups)
			{
				if (typeGroup.Count() > 1)
				{
					foreach (var typeEntry in typeGroup)
					{
						if (!string.IsNullOrEmpty(typeEntry.Name))
							container.RegisterType(typeEntry.ContractType, typeEntry.ImplementationType, typeEntry.Name);
						else
							container.RegisterType(typeEntry.ContractType, typeEntry.ImplementationType);
					}
				}
				else
				{ 
					container.RegisterType(typeGroup.Key, typeGroup.First().ImplementationType);
				}
			}

			return container;
		}

	}
}
