# TEF #

### How is it Licensed? ###

It is licensed using the Apache 2.0 license.  Feel free to do whatever you want with the code; however, the code is provided as is.  Please read the license at https://bitbucket.org/bmains/tef/wiki/License for more information.

### What is this repository for? ###

This project does not replace Dependency Injection containers; it was designed to make wiring up references into a dependency injection container easier.  Types can be marked with attributes and preloaded into the containers.  See the examples below for more details.

### How do I get set up? ###

Install the NUGET package (coming) - choose which version you want: portable or .NET version.

* Install-Package TEF - PCL library
* Install-Package TEFDotNET - .NET PCL and 4.5 extensions

Or Download the source code into your project

### Introduction ###

I’ve always liked the idea of MEF. The ability to mark a class as exportable, then very easily consume it in your application is a very nice feature. However, MEF has always been instance-based, meaning you had to have an instance of a class to register. This works OK in some instances, but not in others. For example, in some instances, an instance of a class in ASP.NET can be problematic, especially if it’s tied to a specific HTTP context.

Because I needed type-specific support, without having to instantiate a specific instance, TEF was born: a simplistic implementation for registering your types. Registering your types is as simple as adding an [ExportType] to your class, or adding an assembly attribute [ExportDependency].

```
#!c#

[ExportType(typeof(ISomeService))]
public class MySomeService : ISomeService
{
}
```

OR:


```
#!c#

[assembly:ExportDependency(typeof(ISomeService), typeof(MySomeService))]
```

To extract the types from the assembly, a specific set of bootstrapping components was setup to make this process easier.  Unfortunately, the way assemblies are parsed differs by environment and thus the entire AppDomain can't be parsed in other environments as it can in a .NET environment.  A sample is below.  Note the project is an MVC project, with a custom App_Start configuration class.

```
#!c#

public class DependencyConfig
{
		public static void Start()
		{
			var container = TypeFactory
				.Configure((c) =>
				{
					c
						.Consumer(new MyDIContainerConsumer())
						.AssemblyLoader(new AppDomainAssemblyLoader());
				})
				.Initialize() as Container;
                }
}
```

These were created to make the process as simple as possible.  The TypeFactory creates the container using a given set of Configuration options, which are customizable.  After that, Initialize() creates the container and returns it.  It uses some components that you need to understand how they are configured:

* Consumer - a class that receives all of the loaded types and loads them into a container.  Check the containers page for more information.
* AssemblyLoader - the assembly loader that looks for the assemblies to load types from.  By default, there is an AppDomainAssemblyLoader that works in the .NET environment, which can also work in Xamarin iOS or Android too.  Otherwise, you can customize and return the assemblies by creating a custom IAssemblyLoader instance, and assigning it here.
* MetadataImporter (optional) - By default, this is the component that finds all of the exported types.  You can customize this process by supplying a metadata importer here.  Generally, that's not needed as the default implementation works fine.

The TypeFactory exists to make building up the container quick.  If you want complete flexibility over the entire process, it's possible using the code shown below.  What's done above is to make it simpler to consume; however, the API is available to you to do whatever you like.  Loading types can be as shown below:

```
#!c#

var importer = new TEFMetadataImporter();
//Pull any assembly dependencies marked with [ExportDependency]
var types = importer.GetAssemblyTypes(AppDomain.CurrentDomain).ToList();
var assemblyTypeCount = types.Count;

//Pull any type dependencies marked with [ExportType]
types.AddRange(importer.GetClassTypes(AppDomain.CurrentDomain));
```

//Types are loaded from the Metadata Importer; now we can load into UI
//Note this is more of a manual way using the API; this is automatically consumed for you above

The benefit to do so is very simply: the ability to register types, instead of instantiating objects, which is a huge plus.  In addition, the framework offers the ability to keep the registration of an interface/class together with the class itself.  This makes it easy to remove dependencies, as the registration and the class are together, rather than the registration being in a separate file.  Also, it prevents a level of excessive reflection, which may or may not exist with other approaches.

Please note as you expand into other environments, reading the entire app domain may not be possible.  In that scenario, an alternative approach of defining types explicitly or loading target assemblies dynamically is the resolution.  That's the caveat with setting up a common solution; the .NET environment offers the most flexibility with loading an entire AppDomain of references.

And that's all there is to it.

### Container-Specific Implementations ###

On this site contains snippets of code to import into your projects.  Why snippets, instead of Nuget packages?  Versions of container change so frequently that it's easier to include the snippets for you to customize and integrate with the version chosen.  You can get an example of snippets at https://bitbucket.org/bmains/tef/wiki/Container_Integration

### What's to Come ###

Container-specific implementations are being created to simplify the loading of types into the container.  Various types of containers will be supported.  Additionally, enhancements are being added to the framework, such as supporting marking resources with specific lifecycle implementations, and more.