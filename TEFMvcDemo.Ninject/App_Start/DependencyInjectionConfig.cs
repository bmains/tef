﻿using Ninject;
using Ninject.Injection;
using Ninject.Web.Common;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TEF.Composition;
using TEF.Consumption;
using Ninject.Planning.Bindings;
using Ninject.Infrastructure;

namespace TEFMvcDemo
{
   public class DependencyInjectionConfig
   {

      public static Type[] GetBindings(IKernel kernel)
      {
         return ((Multimap<Type, IBinding>)typeof(KernelBase)
                .GetField("bindings", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(kernel)).Select(x => x.Key).ToArray();
      }


      public static IKernel Register()
      {
         var container = TypeFactory
             .Configure((c) =>
             {
                c
                    .Consumer(new NinjectConsumer())
                    .AssemblyLoader(new TEF.Consumption.LambdaAssemblyLoader((a) => a.FullName.StartsWith("TEFMvc")));
             })
             .Initialize() as IKernel;


         return container;
      }

   }

   public class NinjectConsumer : BaseConsumer
   {
      protected override object CreateContainer()
      {
         var kernel = new StandardKernel();
         kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
         kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
         
         return kernel;
      }

      

      protected override TypeImplementationScope HandleScopeMismatch(List<TypeImplementationScope> scopes)
      {
         //Temporary
         return scopes.First();
      }

      protected override void RegisterType(object container, Type contractType, Type implementationType, string name, TypeImplementationScope scopeLifecycle)
      {
         try
         {
            var binder = ((IKernel)container).Bind(contractType).To(implementationType);

            if (scopeLifecycle.Scope == LifeTimeScope.Singleton)
               binder.InSingletonScope();
            else if (scopeLifecycle.Scope == LifeTimeScope.PerThread)
               binder.InThreadScope();
            else if (scopeLifecycle.Scope == LifeTimeScope.PerRequest)
               binder.InRequestScope();

            if (!string.IsNullOrEmpty(name))
               binder.Named(name);

            //if (!string.IsNullOrEmpty(name))
            //   ((IKernel)container).Bind(contractType).To(implementationType).Named(name);
            //else
            //   ((IKernel)container).Bind(contractType).To(implementationType);
         }
         catch(Exception ex)
         {
            throw new InvalidOperationException("Error with contract " + contractType.Name + ".", ex);
         }
      }

      protected override void RegisterTypeCollection(object container, Type contractType, string name, IEnumerable<Type> implementations, TypeImplementationScope scopeLifecycle)
      {
         var k = (IKernel)container;

         try
         {
            //Bind 1 to 1, when retrieved will get all
            foreach (var impl in implementations)
            {
               var binder = ((IKernel)container).Bind(contractType).To(impl);

               if (scopeLifecycle.Scope == LifeTimeScope.Singleton)
                  binder.InSingletonScope();
               else if (scopeLifecycle.Scope == LifeTimeScope.PerThread)
                  binder.InThreadScope();
               else if (scopeLifecycle.Scope == LifeTimeScope.PerRequest)
                  binder.InRequestScope();

               if (!string.IsNullOrEmpty(name))
                  binder.Named(name);

               //if (!string.IsNullOrEmpty(name))
               //   k.Bind(contractType).To(impl).Named(name);
               //else
               //   k.Bind(contractType).To(impl);
            }
         }
         catch(Exception ex)
         {
            throw new InvalidOperationException("Error with contract " + contractType.Name + ".", ex);
         }
      }

   }
}