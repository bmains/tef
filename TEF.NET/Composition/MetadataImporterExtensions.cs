﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TEF.Composition
{
    public static class MetadataImporterExtensions
    {

        public static IEnumerable<TypeMapping> GetAssemblyTypes(this IMetadataImporter importer, AppDomain domain)
        {
            var list = new List<TypeMapping>();
            var assemblies = domain.GetAssemblies().ToList();
            assemblies.ForEach((a) =>
            {
                list.AddRange(importer.GetAssemblyTypes(a));
            });

            return list;
        }

        public static IEnumerable<TypeMapping> GetClassTypes(this IMetadataImporter importer, AppDomain domain)
        {
            var list = new List<TypeMapping>();
            var assemblies = domain.GetAssemblies().ToList();

            assemblies.ForEach((a) =>
            {
                list.AddRange(importer.GetClassTypes(a));
            });

            return list;
        }

    }
}
