﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using TEF.Composition;


namespace TEF.Consumption
{
   /// <summary>
   /// A default implementation of <see cref="IAssemblyLoader"/>, which uses a lambda expression to filter the list of assemblies.
   /// </summary>
   public class LambdaAssemblyLoader : IAssemblyLoader
   {
      private Func<Assembly, bool> _lambda = null;



      /// <summary>
      /// Creates the loader with a given lambda expression.
      /// </summary>
      /// <param name="lambda">The lambda expression to filter by.</param>
      public LambdaAssemblyLoader(Func<Assembly, bool> lambda)
      {
         _lambda = lambda;
      }



      /// <summary>
      /// The assemblies in the current appdomain that get filtered by a lambda expression.
      /// </summary>
      /// <returns>The list of assemblies.</returns>
      public IEnumerable<Assembly> LoadAssemblies()
      {
         return AppDomain.CurrentDomain.GetAssemblies().Where(_lambda);
      }

   }
}
