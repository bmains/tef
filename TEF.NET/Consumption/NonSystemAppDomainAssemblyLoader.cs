﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using TEF.Composition;


namespace TEF.Consumption
{
   /// <summary>
   /// A default implementation of <see cref="IAssemblyLoader"/>, which is to filter assemblies that don't start with System.*, Microsoft.*, DotNetOpenAuth.*, WebMatrix.*.
   /// </summary>
   public class NonSystemAppDomainAssemblyLoader : IAssemblyLoader
   {

      /// <summary>
      /// Return the filtered list of assembly references; this method can be overridden to futher refine the list of references you may be looking for.  Called from <see cref="LoadAssemblies"/>.
      /// </summary>
      /// <param name="assemblies">The list of filtered assemblies.</param>
      /// <returns>The final list of assemblies.</returns>
	   protected virtual IEnumerable<Assembly> FilterAssemblies(IEnumerable<Assembly> assemblies)
	   {
		   return assemblies;
	   }

      /// <summary>
      /// Returns a filtered list of assemblies loaded in the AppDomain, where the name doesn't start with one of the aforementioned assembly name prefixes listed with the class.
      /// </summary>
      /// <returns>The list of assemblies.</returns>
	   public IEnumerable<Assembly> LoadAssemblies()
	   {
         return FilterAssemblies(AppDomain.CurrentDomain.GetAssemblies().Where(i =>
            !i.FullName.StartsWith("System") &&
            !i.FullName.StartsWith("Microsoft") &&
            !i.FullName.StartsWith("DotNetOpenAuth") &&
            !i.FullName.StartsWith("WebMatrix") &&
            !i.FullName.StartsWith("Newtonsoft.Json") &&
            !i.FullName.StartsWith("Antlr3") &&
            !i.FullName.StartsWith("EntityFramework")
		   ));
	   }

   }
}
