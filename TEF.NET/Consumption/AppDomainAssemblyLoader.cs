﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using TEF.Composition;


namespace TEF.Consumption
{
   /// <summary>
   /// A default implementation of <see cref="IAssemblyLoader"/>, which loads all of the assemblies in the entire application domain to search for a specific assembly.
   /// </summary>
	public class AppDomainAssemblyLoader : IAssemblyLoader
	{


      /// <summary>
      /// Return the filtered list of assembly references; this method can be overridden to futher refine the list of references you may be looking for.  Called from <see cref="LoadAssemblies"/>.
      /// </summary>
      /// <param name="assemblies">The list of filtered assemblies.</param>
      /// <returns>The final list of assemblies.</returns>
		protected virtual IEnumerable<Assembly> FilterAssemblies(IEnumerable<Assembly> assemblies)
		{
			return assemblies;
		}

      /// <summary>
      /// Returns a filtered list of assemblies loaded in the AppDomain.
      /// </summary>
      /// <returns>The list of assemblies.</returns>
		public IEnumerable<Assembly> LoadAssemblies()
		{
			return FilterAssemblies(AppDomain.CurrentDomain.GetAssemblies());
		}

	}
}
