﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEFMvcDemo;


namespace TEFMvcDemo.Controllers
{
    public class ScopingController : Controller
    {
      private IRequestTimeService _rqTime = null;
      private ISingletonTimeService _singletonTime = null;


      /// <summary>
      /// Since these two objects are scoped differently, you'll see a disparity in the time reported; since the time is stored
      /// in a variable defined in the constructor, the singleton's constructor is only called once, where the per request service
      /// recreates every ASP.NET request.
      /// </summary>
      /// <param name="rqTime">The per request service to report time - updates every time.</param>
      /// <param name="singTime">The singleton service to report time - only reports one time.</param>

      public ScopingController(IRequestTimeService rqTime, ISingletonTimeService singTime)
      {
         _rqTime = rqTime;
         _singletonTime = singTime;
      }



        public ActionResult Index()
        {
            var model = new Models.ScopingIndexModel();
            model.PerRequestTime = _rqTime.GetTime();
            model.GlobalTime = _singletonTime.GetTime();

            return View(model);
        }
    }
}