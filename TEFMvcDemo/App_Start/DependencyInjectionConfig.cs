﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

using TEF.Composition;
using TEF.Consumption;


namespace TEFMvcDemo
{
   /// <summary>
   /// I like to follow the App_Start approach to isolate the Dependency Injection container buildup;
   /// it is not a requirement though.  Feel free to use whatever approach you want. 
   /// </summary>
   /// <remarks>This class gets called from Global.asax.</remarks>
	public class DependencyInjectionConfig
	{

		public static void Start()
		{
         TEFDemoMarker.Init();

         //Because each container has a widely different way of setting itself up and differently named methods and approaches
         //to registering types, this approach had to be as flexible as possible to maintain a wider horizontal growth across
         //the container spectrum.  This one framework should be able to adapt to most containers on the .NET market.
         //The idea is simple; collect all of the types and dependencies dynamically, and pass them along to the container.
         //This does not replace the DI container at all.

         //The 2 custom pieces of information needed are:
         //The assembly loader, which finds assemblies to search for marked types. There are 2 internal implementations, one that looks
         var container = TypeFactory
			 .Configure((c) =>
			 {
             //The consumer wraps around the container desired to be used; it can inherit from IConsumer, and implement all of the registration
             //logic manually, or inherit BaseConsumer, which takes care of the common plumbing, but needs a little help in setting up
             //the container and the lifetime scoping.

             c.Consumer(new SimpleInjectorConsumer())
                .AssemblyLoader(new TEF.Consumption.NonSystemAppDomainAssemblyLoader());

             //The assembly loader simply finds the assemblies to look in.  The idea is to look for assemblies with specific names, so the 
             //system is not including Microsoft or other system DLLs.  This makes the retrieval process more efficient.

             //Sample: .AssemblyLoader(new AppDomainAssemblyLoader()); - Note this class can be inherited from and have the FilterAssemblies method overwritten to filter the list down
             //Sample: .AssemblyLoader(new LambdaAssemblyLoader((a) => { return a.FullName.StartsWith("TEFMvc"); }));
          })
			 .Initialize() as SimpleInjector.Container;

			//container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

			// This is an extension method from the integration package.
			container.RegisterMvcControllers(System.Reflection.Assembly.GetExecutingAssembly());

			container.Verify();

			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
		}

	}


   /// <summary>
   /// An example of a base consumer class that wraps around the SimpleInjector DI container.
   /// </summary>
	public class SimpleInjectorConsumer : TEF.Consumption.BaseConsumer
	{
      /// <summary>
      /// Creates the SimpleInjector container.
      /// </summary>
      /// <returns>The instantiated container.</returns>
		protected override object CreateContainer()
		{
			return new SimpleInjector.Container();
		}

      /// <summary>
      /// Translates the internal <see cref="LifeTimeScope"/> enum into the final Lifestyle enum.
      /// </summary>
      /// <param name="registration">The current registration.</param>
      /// <returns>The final scope.</returns>
		private Lifestyle ConvertLifeTimeScope(TypeImplementationScope scopeLifecycle)
		{
         var scope = scopeLifecycle.Scope;

			if (scope == LifeTimeScope.Singleton)
				return Lifestyle.Singleton;
			else if (scope == LifeTimeScope.Transient)
				return Lifestyle.Transient;
			else if (scope == LifeTimeScope.PerThread)
				return new SimpleInjector.Lifestyles.ThreadScopedLifestyle();
			else if (scope == LifeTimeScope.PerRequest)
				return new WebRequestLifestyle();
			else if (scope == LifeTimeScope.Custom)
			{
				if (scopeLifecycle.CustomScopeName == "ASYNC")
					return new SimpleInjector.Lifestyles.AsyncScopedLifestyle();
			}

			return null;
		}

      protected override TypeImplementationScope HandleScopeMismatch(List<TypeImplementationScope> scopes)
      {
         //Temp
         return scopes[0];
      }

      /// <summary>
      /// Registers a collection of implementation types into the DI container.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="contractType"></param>
      /// <param name="implementationTypes"></param>
      /// <param name="name"></param>
      /// <param name="lifecycle"></param>
      /// <remarks>SimpleInjector doesn't support Named registration, so names can't be used.</remarks>
		protected override void RegisterTypeCollection(object container, Type contractType, string name, IEnumerable<Type> implementations, TypeImplementationScope scope)
		{
			//It's up to each container (most normally do) to determine whether they support named registration. 

         //All of this logic is specific to the simple injector container API.
			var ctor = (SimpleInjector.Container)container;
			
			ctor.RegisterCollection(contractType, 
				from t in implementations
            select this.ConvertLifeTimeScope(scope).CreateRegistration(contractType, () => Activator.CreateInstance(t), ctor));
		}

      /// <summary>
      /// Registers a type implementation into the SimpleInjector container.
      /// </summary>
      /// <param name="container"></param>
      /// <param name="contractType"></param>
      /// <param name="implementationType"></param>
      /// <param name="name"></param>
      /// <param name="lifecycle"></param>
      /// <remarks>SimpleInjector doesn't support Named registration, so names can't be used.</remarks>
		protected override void RegisterType(object container, Type contractType, Type implementationType, string name, TypeImplementationScope lifecycle)
		{
			var ctor = (SimpleInjector.Container)container;
         var lifestyle = this.ConvertLifeTimeScope(lifecycle);

			ctor.Register(contractType, implementationType, lifestyle);
		}
	}

   /// <summary>
   /// If you want to manually handle all of the registration mapping capabilities, then you certainly can.  But it's easier
   /// to use the approach above (inheriting from BaseConsumer), which handles mapping groupings and such.
   /// </summary>
	public class OldSimpleInjectorConsumer : IConsumer
	{
		public object Consume(IEnumerable<TypeMapping> types)
		{
         var container = new Container();

         foreach (var typeMapping in types)
         {
            if (typeMapping.Implementations != null && typeMapping.Implementations.Count > 1)
            {
               var scopes = typeMapping.Implementations.Select(i => i.ScopeLifetime).Distinct().ToList();
               if (scopes.Count > 1)
                  throw new InvalidOperationException("When registering collections, cannot mix scope.");

               container.RegisterCollection(typeMapping.ContractType,
                  from t in typeMapping.Implementations
                  select this.ConvertLifeTimeScope(scopes[0]).CreateRegistration(typeMapping.ContractType, () => Activator.CreateInstance(t.ImplementationType), container));
            }
            else
            {
               var impl = typeMapping.Implementations[0];
               container.Register(typeMapping.ContractType, impl.ImplementationType, this.ConvertLifeTimeScope(impl.ScopeLifetime));
            }
         }
         
			return container;
		}



		private Lifestyle ConvertLifeTimeScope(TypeImplementationScope scopeLifecycle)
		{
         var scope = scopeLifecycle.Scope;

			if (scope == LifeTimeScope.Singleton)
				return Lifestyle.Singleton;
			else if (scope == LifeTimeScope.Transient)
				return Lifestyle.Transient;
			else if (scope == LifeTimeScope.PerThread)
				return Lifestyle.Scoped;
			else if (scope == LifeTimeScope.PerRequest)
				return new WebRequestLifestyle();
			else
				return null;
		}
   }


}