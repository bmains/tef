﻿using System;
using System.Collections.Generic;
using System.Linq;

using SimpleInjector;
using TEF.Composition;


namespace TEF.Consumption
{
	public class SimpleInjectorConsumer : IConsumer
	{
		public object Consume(IEnumerable<TypeMapping> types)
		{
			var	container = new Container();

			//Can also group types, if you have more than one using the same signature, using something like:
			var typeGroups = types.GroupBy(i => i.ContractType);
			foreach (var typeGroup in typeGroups)
			{
				if (typeGroup.Count() > 1)
					container.RegisterAll(typeGroup.Key, typeGroup.Select(i => i.ImplementationType));
				else
					container.Register(typeGroup.Key, typeGroup.First().ImplementationType);
			}

			return container;
		}

	}
}
