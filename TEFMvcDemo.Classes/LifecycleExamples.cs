﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TEF.Composition;



namespace TEFMvcDemo
{
	
	public interface IRequestTimeService
	{

		string GetTime();

	}

	[ExportType(typeof(IRequestTimeService), LifeTimeScope.PerRequest)]
	public class AspRequestTimeService : IRequestTimeService
	{
		private string _time;



		public AspRequestTimeService()
		{
			_time = DateTime.Now.ToString();
		}


		public string GetTime()
		{
			return _time;
		}
	}


	public interface ISingletonTimeService
	{

		string GetTime();

	}

	[ExportType(typeof(ISingletonTimeService), LifeTimeScope.Singleton)]
	public class AspSingletonTimeService : ISingletonTimeService
	{
		private string _time;



		public AspSingletonTimeService()
		{
         //Since this is a singleton, this variable will be cached here; with singletons, you have
         //to be aware what gets created in the constructor, to make sure we don't store globally
         //what's really meant to be either transient or per-request (like references to Http Context,
         //Database connections, EF Contexts, etc.)
			_time = DateTime.Now.ToString();
		}


		public string GetTime()
		{
			return _time;
		}
	}

	public interface IThreadTimeService
	{

		string GetTime();

	}

	[ExportType(typeof(IThreadTimeService), "ASYNC")]
	public class AspThreadTimeService : IThreadTimeService
	{
		private string _time;



		public AspThreadTimeService()
		{
			_time = DateTime.Now.ToString();
		}


		public string GetTime()
		{
			return _time;
		}
	}

}