﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly:TEF.Composition.ExportDependency(typeof(TEFMvcDemo.IStateOptionsService), typeof(TEFMvcDemo.AKStateOptionsService))]
[assembly: TEF.Composition.ExportDependency(typeof(TEFMvcDemo.IStateOptionsService), typeof(TEFMvcDemo.ALStateOptionsService))]
[assembly: TEF.Composition.ExportDependency(typeof(TEFMvcDemo.IStateOptionsService), typeof(TEFMvcDemo.INStateOptionsService))]
[assembly: TEF.Composition.ExportDependency(typeof(TEFMvcDemo.IStateOptionsService), typeof(TEFMvcDemo.ILStateOptionsService))]

namespace TEFMvcDemo
{
	
	public interface IStateOptionsService
	{

		bool IsForState(string name);

		object GetOptions();

	}

	public class AKStateOptionsService : IStateOptionsService
	{

		public bool IsForState(string name)
		{
			return name == "AK";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	public class ALStateOptionsService : IStateOptionsService
	{

		public bool IsForState(string name)
		{
			return name == "AL";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	public class INStateOptionsService : IStateOptionsService
	{

		public bool IsForState(string name)
		{
			return name == "IN";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	public class ILStateOptionsService : IStateOptionsService
	{

		public bool IsForState(string name)
		{
			return name == "IL";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

}