﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using TEF.Composition;


namespace TEFMvcDemo
{

	public interface ICountyOptionsService
	{

		bool IsForCounty(string name);

		object GetOptions();

	}

	[ExportType(typeof(ICountyOptionsService))]
	public class AKCountyOptionsService : ICountyOptionsService
	{

		public bool IsForCounty(string name)
		{
			return name == "001";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	[ExportType(typeof(ICountyOptionsService))]
	public class ALCountyOptionsService : ICountyOptionsService
	{

		public bool IsForCounty(string name)
		{
			return name == "002";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	[ExportType(typeof(ICountyOptionsService))]
	public class INCountyOptionsService : ICountyOptionsService
	{

		public bool IsForCounty(string name)
		{
			return name == "003";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

	[ExportType(typeof(ICountyOptionsService))]
	public class ILCountyOptionsService : ICountyOptionsService
	{

		public bool IsForCounty(string name)
		{
			return name == "004";
		}

		public object GetOptions()
		{
			return new object();
		}
	}

}