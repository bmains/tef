﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TEF.Composition;

// *************************************************
// Examples of ExportDependency and ExportType.  There is no essential difference between the two.
// The idea here is to illustrate assembly attributes can be used in multitude if needed; otherwise,
// falling back to looping through assembly type definitions is OK.
// *************************************************

namespace TEFMvcDemo
{
	
	// **********************************************************************
	// Example ExportType examples

	public interface IHttpContextService
	{

		HttpContextBase GetContext();

	}

	[ExportType(typeof(IHttpContextService))]
	public class HttpContextService : IHttpContextService
	{

		public HttpContextBase GetContext()
		{
			return new HttpContextWrapper(HttpContext.Current);
		}

	}


	public interface ISessionService
	{
		string Get(string key);
	}

	[ExportType(typeof(ISessionService))]
	public class AspSessionService : ISessionService
	{

		public string Get(string key)
		{
			var val = HttpContext.Current.Session[key];
			return (val != null) ? val.ToString() : null;
		}
	}




}