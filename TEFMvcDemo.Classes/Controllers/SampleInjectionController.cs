﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using TEFMvcDemo;


namespace TEFMvcDemo.Controllers
{
    public class SampleInjectionController : Controller
    {
      private TEFMvcDemo.ICacheService _cacheService = null;
      private TEFMvcDemo.ISessionService _sessionService = null;
      //List support for one-to-many is supported; how you consume the references may vary by container
      private IList<TEFMvcDemo.IStateOptionsService> _stateService = null;



      /// <summary>
      /// The container is doing all the work here; this is to demo that TEF did correctly import the singletons and lists of mappings.
      /// </summary>
      /// <param name="cacheService"></param>
      /// <param name="sessionService"></param>
      /// <param name="stateService"></param>
      public SampleInjectionController(ICacheService cacheService, ISessionService sessionService, IList<TEFMvcDemo.IStateOptionsService> stateService)
      {
         _cacheService = cacheService;
         _sessionService = sessionService;
         _stateService = stateService;
      }




        public ActionResult Index()
        {
            
            return View();
        }

    }
}