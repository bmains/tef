﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TEFMvcDemo.Models
{
   public class ScopingIndexModel
   {

      public string PerRequestTime { get; set; }

      public string GlobalTime { get; set; }


   }
}