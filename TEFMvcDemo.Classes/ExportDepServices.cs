﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TEF.Composition;

// *************************************************
// Examples of ExportDependency and ExportType.  There is no essential difference between the two.
// The idea here is to illustrate assembly attributes can be used in multitude if needed; otherwise,
// falling back to looping through assembly type definitions is OK.
// *************************************************

//ExportDependency Examples
[assembly: ExportDependency(typeof(TEFMvcDemo.ICacheService), typeof(TEFMvcDemo.AspCacheService))]
[assembly: ExportDependency(typeof(TEFMvcDemo.ILogService), typeof(TEFMvcDemo.EventLogService))]

namespace TEFMvcDemo
{
	// **********************************************************************
	// Example ExportDependency examples as defined above

	public interface ICacheService
	{
		object Get(string key);
	}

   public class AspCacheService : ICacheService
	{

		public object Get(string key)
		{
			return HttpContext.Current.Cache.Get(key);
		}
	}

	public interface ILogService
	{

		void LogMessage(string category, string msg);

		void LogError(string category, Exception ex);

	}

	public class EventLogService : ILogService
	{

		public void LogMessage(string category, string msg)
		{
			System.Diagnostics.EventLog.WriteEntry(category, msg, System.Diagnostics.EventLogEntryType.Information);
		}

		public void LogError(string category, Exception ex)
		{
			System.Diagnostics.EventLog.WriteEntry(category, ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
		}

	}
	

}